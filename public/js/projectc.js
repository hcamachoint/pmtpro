//VALIDA QUE LOS INPUT NUMBER NO PUEDAN TENER SCROLL CON LA RUEDA DEL RATON
$(document).on("wheel", "input[type=number]", function (e) {
    $(this).blur();
});

$(document).ready(function () {
    //MATERIALS
    var counter = document.getElementById("pm").value;
    if (counter != 0) {
      counter += 1;
    }

    $("#addmat").on("click", function () {
        var newRow = $("<tr>");
        var cols = "";
        cols += '<td><input type="text" class="form-control" name="material_description[]' + counter + '" required/></td>';
        /*cols += '<td><input name="material_attachment[]" type="file"></td>';*/
        cols += '<td><span class="input-dollar left"><input id="material_price_' + counter + '" onkeyup="materialCalc(' + counter + ')" onchange="decimable(`material_price_' + counter + '`)" class="form-control materialprice" name="material_price[]' + counter + '" type=text step=any required/></span></td>';
        cols += '<td><input id="material_count_' + counter + '" onkeyup="materialCalc(' + counter + ')" type="number" step=any class="form-control" name="material_count[]' + counter + '" required/></td>';
        cols += '<td><input class="calculable form-control" id="totmat_' + counter + '" value="0" disabled></td>';
        cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>';
        newRow.append(cols);
        $("table.order-list").append(newRow);
        counter++;
    });
    //BORRA ROW DE MATERIALS
    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();
        counter -= 1;
        calculateGrandTotal();
    });
    //LABORS
    var counter2 = document.getElementById("pl").value;
    if (counter2 != 0) {
      counter2 += 1;
    }
    $("#addlab").on("click", function () {
        var newRow2 = $("<tr>");
        var cols2 = "";
        var ro = document.getElementById("sel");
        cols2 += '<td>' + ro.innerHTML + '</td>';
        cols2 += '<td><span class="input-dollar left"><input id="worker_price_' + counter2 + '" onkeyup="workerCalc(' + counter2 + ')" onchange="decimable(`worker_price_' + counter2 + '`)" type="text" class="form-control workerprice" name="worker_price[]' + counter2 + '" required/></span></td>';
        cols2 += '<td><input id="worker_hours_' + counter2 + '" onkeyup="workerCalc(' + counter2 + ')" type="number" step=any class="form-control" name="worker_hours[]' + counter2 + '" required/></td>';
        cols2 += '<td><input class="calculable form-control"  id="totlab_' + counter2 + '" value="0" disabled></td>';
        cols2 += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>';
        newRow2.append(cols2);
        $("table.order-list2").append(newRow2);
        counter2++;
    });
    //BORRA ROW DE LABORS
    $("table.order-list2").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();
        counter -= 1;
        calculateGrandTotal();
    });
    originalBudget();
});

function materialCalc(counter){
  var cleave = new Cleave('.materialprice', {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
  });

  var fila_material_price = document.getElementById("material_price_"+counter).value;
  var fila_material_count = document.getElementById("material_count_"+counter).value;

  var filmatpri = parseFloat(fila_material_price.replace(/,/g, ''));
  var filmatcou = parseFloat(fila_material_count.replace(/,/g, ''));

  var totalmat = filmatpri * filmatcou;
  document.getElementById("totmat_"+counter).value = totalmat;

  var cleave = new Cleave('#totmat_'+counter, {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
  });
  var cleave2 = new Cleave('#material_price_'+counter, {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
  });

  originalBudget();
  calculateGrandTotal();
}

function workerCalc(counter){
  var fila_workers = document.getElementById("worker_price_"+counter).value * document.getElementById("worker_hours_"+counter).value;
  document.getElementById("totlab_"+counter).value = fila_workers;


  var cleave = new Cleave('.workerprice', {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
  });

  var fila_worker_price = document.getElementById("worker_price_"+counter).value;
  var fila_worker_hours_ = document.getElementById("worker_hours_"+counter).value;

  var fillabpri = parseFloat(fila_worker_price.replace(/,/g, ''));
  var filmatcou = parseFloat(fila_worker_hours_.replace(/,/g, ''));

  var totallab = fillabpri * filmatcou;
  document.getElementById("totlab_"+counter).value = totallab;

  var cleave = new Cleave('#totlab_'+counter, {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
  });
  var cleave2 = new Cleave('#worker_price_'+counter, {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
  });

  calculateGrandTotal();
}

function originalBudget(){
  var budget = document.getElementById("budget").value;
  if (budget) {
    document.getElementById("originBudget").value = prices(budget);
  }else{
    document.getElementById("originBudget").value = '0.00';
  }
  calculateGrandTotal();
}

function calculateGrandTotal() {
    var costos = 0;
    var profit = 0;
    var budget = document.getElementById("budget").value;

    budget = budget.replace(/,/g, '');
    if (!budget) {budget=0;}else{budget = parseFloat(budget)}

    var sumaRows = document.getElementsByClassName("calculable");
    for(var i=0; i<sumaRows.length; i++)
    {
        valor = sumaRows[i].value;
        valor = valor.replace(/,/g, '');
        if (!valor) {
          valor = 0;
        }
        costos += parseFloat(valor);
    }

    netProfit = budget - costos;
    netProfitMargin = (netProfit * 100) / budget;


    document.getElementById("netProfit").value = netProfit.toFixed(2);
    var cleavepro = new Cleave('#netProfit', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand'
    });

    if (budget != 0) {
      document.getElementById("netProfitMargin").value = netProfitMargin.toFixed(2);
      var cleavepro = new Cleave('#netProfitMargin', {
          numeral: true,
          numeralThousandsGroupStyle: 'thousand'
      });
    }else {
      document.getElementById("netProfitMargin").value = 0;
    }
}

function workerCreate(){
  //var firstname = $('input[name=firstname]').val();
  //var lastname = $('input[name=lastname]').val();
  //data: {_token: token, firstname: firstname, lastname: lastname}
  $.ajax({
      type: 'POST',
      url: "http://"+window.location.hostname+"/project/worker/",
      contentType: "application/json",
      data: JSON.stringify({ "name": $('#labname').val(), "address":$('#labaddress').val(), "city":$('#labcity').val() }),
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(msg){
          $('#createWorker').modal('hide');
          $('#labname').val('');
          $('#labaddress').val('');
          $('#labcity').val('');
          $('.labList').append($('<option>', {
            value: msg.worker.id,
            text: msg.worker.name
          }));
          //sortSelect('.labList', 'text', 'asc');
          /*document.getElementById("workerCreator").style.visibility = "visible";*/
      },
      error: function(msg){
          console.log(msg);
      }
  });
}

function customerCreate(){
  //var firstname = $('input[name=firstname]').val();
  //var lastname = $('input[name=lastname]').val();
  //data: {_token: token, firstname: firstname, lastname: lastname}
  $.ajax({
      type: 'POST',
      url: "http://"+window.location.hostname+"/project/customer/",
      contentType: "application/json",
      data: JSON.stringify({ "name": $('#cusname').val(), "address":$('#cusaddress').val(), "city":$('#cuscity').val() }),
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(msg){
          $('#createCustomer').modal('hide');
          $('#cusname').val('');
          $('#cusaddress').val('');
          $('#cuscity').val('');
          $('.cusList').append($('<option>', {
            value: msg.customer.id,
            text: msg.customer.name
          }));
          sortSelect('.cusList', 'text', 'asc');
      },
      error: function(msg){
          console.log(msg);
      }
  });
}

//, attr, order

var sortSelect = function (select, attr, order) {
    if(attr === 'text'){
        if(order === 'asc'){
            $(select).html($(select).children('option').sort(function (x, y) {
                return $(x).text() < $(y).text() ? -1 : 1;
            }));
            $(select).get(0).selectedIndex = 0;
            e.preventDefault();
        }// end asc
        if(order === 'desc'){
            $(select).html($(select).children('option').sort(function (y, x) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $(select).get(0).selectedIndex = 0;
            e.preventDefault();
        }// end desc
    }

};

function commify(input){
  var cleave = new Cleave(input, {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
  });
}
