//VALIDA QUE LOS INPUT NUMBER NO PUEDAN TENER SCROLL CON LA RUEDA DEL RATON
$(document).on("wheel", "input[type=number]", function (e) {
    $(this).blur();
});

/*$("#budget").change(function(){
  var val = parseInt($('#budget').text());
  val = numberWithCommas(val);
  $('#budget').text(val);
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}*/

$(document).ready(function () {
    //MATERIALS
    var counter = document.getElementById("pm").value;
    if (counter != 0) {
      counter += 1;
    }

    $('.commifyble').toArray().forEach(function(field){
       new Cleave(field, {
         numeral: true,
         numeralThousandsGroupStyle: 'thousand'
       })
    });

    $("#addmat").on("click", function () {
        var newRow = $("<tr>");
        var cols = "";
        cols += '<td><input type="text" class="form-control" name="material_description[]' + counter + '" required/></td>';
        /*cols += '<td><input name="material_attachment[]" type="file"></td>';*/
        cols += '<td><span class="input-dollar left"><input id="material_price_' + counter + '" onkeyup="materialCalc(' + counter + ')" class="form-control materialprice" name="material_price[]' + counter + '" type=text step=any required/></span></td>';
        cols += '<td><input id="material_count_' + counter + '" onkeyup="materialCalc(' + counter + ')" type="number" step=any class="form-control" name="material_count[]' + counter + '" required/></td>';
        cols += '<td><input class="calculable form-control" id="totmat_' + counter + '" value="0" disabled></td>';
        cols += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>';
        newRow.append(cols);
        $("table.order-list").append(newRow);
        counter++;
    });
    //BORRA ROW DE MATERIALS
    $("table.order-list").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();
        counter -= 1;
        calculateGrandTotal();
    });
    //LABORS
    var counter2 = document.getElementById("pl").value;
    if (counter2 != 0) {
      counter2 += 1;
    }
    $("#addlab").on("click", function () {
        var newRow2 = $("<tr>");
        var cols2 = "";
        var ro = document.getElementById("sel");
        cols2 += '<td>' + ro.innerHTML + '</td>';
        cols2 += '<td><span class="input-dollar left"><input id="labor_price_' + counter2 + '" onkeyup="laborCalc(' + counter2 + ')" type="number" class="form-control" name="labor_price[]' + counter2 + '" required/></span></td>';
        cols2 += '<td><input id="labor_hours_' + counter2 + '" onkeyup="laborCalc(' + counter2 + ')" type="number" step=any class="form-control" name="labor_hours[]' + counter2 + '" required/></td>';
        cols2 += '<td><input class="calculable form-control"  id="totlab_' + counter2 + '" value="0" disabled></td>';
        cols2 += '<td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>';
        newRow2.append(cols2);
        $("table.order-list2").append(newRow2);
        counter2++;
    });
    //BORRA ROW DE LABORS
    $("table.order-list2").on("click", ".ibtnDel", function (event) {
        $(this).closest("tr").remove();
        counter -= 1;
        calculateGrandTotal();
    });
    originalBudget();
    calculateGrandTotal();
});

function materialCalc(counter){
  var cleave = new Cleave('.materialprice', {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
  });

  var fila_material_price = document.getElementById("material_price_"+counter).value;
  var fila_material_count = document.getElementById("material_count_"+counter).value;

  var filmatpri = parseFloat(fila_material_price.replace(/,/g, ''));
  var filmatcou = parseFloat(fila_material_count.replace(/,/g, ''));

  var totalmat = filmatpri * filmatcou;
  document.getElementById("totmat_"+counter).value = totalmat;

  var cleave = new Cleave('#totmat_'+counter, {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
  });

  calculateGrandTotal();
}

function laborCalc(counter){
  var fila_labors = document.getElementById("labor_price_"+counter).value * document.getElementById("labor_hours_"+counter).value;
  document.getElementById("totlab_"+counter).value = fila_labors;
  calculateGrandTotal();
  //console.log(fila_labors);
}

function originalBudget(){
  var budget = document.getElementById("budget").value;
  console.log(budget);
  document.getElementById("originBudget").innerHTML = budget;
  calculateGrandTotal();
  //console.log(budget);
}

function calculateGrandTotal() {
    var costos = 0;
    var profit = 0;
    var budget = document.getElementById("budget").value;
    var sumaRows = document.getElementsByClassName("calculable");
    for(var i=0; i<sumaRows.length; i++)
    {
        costos += parseInt(sumaRows[i].value);
    }

    netProfit = budget - costos;
    netProfitMargin = (netProfit * 100) / budget;
    document.getElementById("netProfit").innerText = netProfit + ' $';
    if (budget != 0) {
      document.getElementById("netProfitMargin").innerText = netProfitMargin + ' %';
    }else {
      document.getElementById("netProfitMargin").innerText = 0 + ' %';
      //document.getElementById("netProfit").innerText = 0 + ' $';
    }
}

function laborCreate(){
  //var firstname = $('input[name=firstname]').val();
  //var lastname = $('input[name=lastname]').val();
  //data: {_token: token, firstname: firstname, lastname: lastname}
  $.ajax({
      type: 'POST',
      url: "http://"+window.location.hostname+"/project/labor/",
      contentType: "application/json",
      data: JSON.stringify({ "name": $('#labname').val(), "address":$('#labaddress').val(), "city":$('#labcity').val() }),
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(msg){
          $('#createLabor').modal('hide');
          $('#labname').val('');
          $('#labaddress').val('');
          $('#labcity').val('');
          $('.labList').append($('<option>', {
            value: msg.labor.id,
            text: msg.labor.name
          }));
          //sortSelect('.labList', 'text', 'asc');
          /*document.getElementById("laborCreator").style.visibility = "visible";*/
      },
      error: function(msg){
          console.log(msg);
      }
  });
}

function customerCreate(){
  //var firstname = $('input[name=firstname]').val();
  //var lastname = $('input[name=lastname]').val();
  //data: {_token: token, firstname: firstname, lastname: lastname}
  $.ajax({
      type: 'POST',
      url: "http://"+window.location.hostname+"/project/customer/",
      contentType: "application/json",
      data: JSON.stringify({ "name": $('#cusname').val(), "address":$('#cusaddress').val(), "city":$('#cuscity').val() }),
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(msg){
          $('#createCustomer').modal('hide');
          $('#cusname').val('');
          $('#cusaddress').val('');
          $('#cuscity').val('');
          $('.cusList').append($('<option>', {
            value: msg.customer.id,
            text: msg.customer.name
          }));
          sortSelect('.cusList', 'text', 'asc');
      },
      error: function(msg){
          console.log(msg);
      }
  });
}

//, attr, order

var sortSelect = function (select, attr, order) {
    if(attr === 'text'){
        if(order === 'asc'){
            $(select).html($(select).children('option').sort(function (x, y) {
                return $(x).text() < $(y).text() ? -1 : 1;
            }));
            $(select).get(0).selectedIndex = 0;
            e.preventDefault();
        }// end asc
        if(order === 'desc'){
            $(select).html($(select).children('option').sort(function (y, x) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $(select).get(0).selectedIndex = 0;
            e.preventDefault();
        }// end desc
    }

};

function commify(input){
  var cleave = new Cleave(input, {
      numeral: true,
      numeralThousandsGroupStyle: 'thousand'
  });
}
