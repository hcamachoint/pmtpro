$('.datepicker').datetimepicker({
  icons: {
      time: "fa fa-clock-o",
      date: "fa fa-calendar",
      up: "fa fa-arrow-up",
      down: "fa fa-arrow-down",
      previous: "fa fa-caret-left",
      next: "fa fa-caret-right",
  },
  format: 'MM/DD/YYYY'
});
