function retr_dec(numStr) {
    var pieces = numStr.split(".");
    return pieces[1].length;
}

function prices(price){
  var dec = price;
  dec = dec.toString();
  var lastChar = dec.substr(-1);
  if (dec.indexOf(".")==-1) {
    dec = dec + '.00';
  }else{
    if (retr_dec(dec) == 1) {
      if (dec.indexOf(".") >= 0 && lastChar >= 1) {
        dec = dec + '0';
      }
    }
  }
  return dec;
}

function round(value, exp) {
  if (typeof exp === 'undefined' || +exp === 0)
    return Math.round(value);

  value = +value;
  exp = +exp;

  if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
    return NaN;

  // Shift
  value = value.toString().split('e');
  value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

  // Shift back
  value = value.toString().split('e');
  return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}

function decimable(id){

  var dec = document.getElementById(id).value;
  if (dec) {dec = document.getElementById(id).value;dec = dec.replace(/,/g, '');}else{dec = 0}

  if (dec!=0) {
    //console.log(dec % 1 == 0)
    //dec % 1 == 0
    var res = dec.split(".");

    //console.log(res);
    //console.log(dec.indexOf("."));

    if (res[1] == '' || res[1] == null) {
    	dec = dec + '.00';
      document.getElementById(id).value = prices(parseFloat(dec));
    }else if (res[1].length == 1) {
      dec = dec + '0';
      document.getElementById(id).value = prices(parseFloat(dec));
    }else{
      if (retr_dec(dec) == 3) {
        var lastChar = dec[dec.length - 1];
        if (lastChar == '0') {
          dec = dec.substring(0, dec.length - 1);
          document.getElementById(id).value = prices(parseFloat(dec));
        }
      }
    }

    var cleave = new Cleave('#'+id, {
        numeral: true,
        numeralDecimalScale: 3,
        numeralThousandsGroupStyle: 'thousand'
    });

  }
}
