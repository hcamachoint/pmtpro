<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $uadmin = DB::table('users')->insert([
        'name' => 'Admin',
        'sex' => 'male',
        'email' => 'admin@local.dev',
        'password' => bcrypt('asd123'),
        'type' => 1,
        'created_at' => Carbon\Carbon::now(),
        'updated_at' => Carbon\Carbon::now(),
      ]);
    }
}
