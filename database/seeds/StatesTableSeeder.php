<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('states')->insert([
        'title' => 'Florida',
        'slug' => 'florida',
        'country_id' => 187,
      ]);

      DB::table('states')->insert([
        'title' => 'Texas',
        'slug' => 'texas',
        'country_id' => 187,
      ]);

      DB::table('states')->insert([
        'title' => 'New York',
        'slug' => 'new_york',
        'country_id' => 187,
      ]);

      DB::table('states')->insert([
        'title' => 'California',
        'slug' => 'california',
        'country_id' => 187,
      ]);
    }
}
