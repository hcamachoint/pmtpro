<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
          'title' => 'Miami',
          'slug' => 'miami',
          'state_id' => 1,
        ]);

        DB::table('cities')->insert([
          'title' => 'Fort Laurdale',
          'slug' => 'fort_laurdale',
          'state_id' => 1,
        ]);

        DB::table('cities')->insert([
          'title' => 'Orlando',
          'slug' => 'orlando',
          'state_id' => 1,
        ]);

        DB::table('cities')->insert([
          'title' => 'Tampas',
          'slug' => 'tampas',
          'state_id' => 1,
        ]);

        DB::table('cities')->insert([
          'title' => 'Talajasi',
          'slug' => 'talajasi',
          'state_id' => 1,
        ]);

        DB::table('cities')->insert([
          'title' => 'Talas',
          'slug' => 'talas',
          'state_id' => 2,
        ]);

        DB::table('cities')->insert([
          'title' => 'Houston',
          'slug' => 'houston',
          'state_id' => 2,
        ]);

        DB::table('cities')->insert([
          'title' => 'San antonio',
          'slug' => 'san_antonio',
          'state_id' => 2,
        ]);

        DB::table('cities')->insert([
          'title' => 'Austin',
          'slug' => 'austin',
          'state_id' => 2,
        ]);

        DB::table('cities')->insert([
          'title' => 'New York City',
          'slug' => 'new_york_city',
          'state_id' => 3,
        ]);

        DB::table('cities')->insert([
          'title' => 'Bronx',
          'slug' => 'bronx',
          'state_id' => 3,
        ]);

        DB::table('cities')->insert([
          'title' => 'Manhattan',
          'slug' => 'manhattan',
          'state_id' => 3,
        ]);

        DB::table('cities')->insert([
          'title' => 'Brooklyn',
          'slug' => 'brooklyn',
          'state_id' => 3,
        ]);

        DB::table('cities')->insert([
          'title' => 'Queens',
          'slug' => 'queens',
          'state_id' => 3,
        ]);

        DB::table('cities')->insert([
          'title' => 'Los Angeles',
          'slug' => 'los_angeles',
          'state_id' => 4,
        ]);

        DB::table('cities')->insert([
          'title' => 'San Francisco',
          'slug' => 'san_francisco',
          'state_id' => 4,
        ]);

        DB::table('cities')->insert([
          'title' => 'San Diego',
          'slug' => 'san_diego',
          'state_id' => 4,
        ]);

        DB::table('cities')->insert([
          'title' => 'Venice Beach',
          'slug' => 'venice_beach',
          'state_id' => 4,
        ]);

        DB::table('cities')->insert([
          'title' => 'Sacramento',
          'slug' => 'sacramento',
          'state_id' => 4,
        ]);
    }
}
