<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectMaterialsTable extends Migration {

	public function up()
	{
		Schema::create('project_materials', function(Blueprint $table) {
			$table->increments('id');
			$table->text('description');
			$table->decimal('price', 12,4);
			$table->float('count');
			$table->string('attachment')->nullable();
			$table->integer('project_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('project_materials');
	}
}
