<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectWorkersTable extends Migration {

	public function up()
	{
		Schema::create('project_workers', function(Blueprint $table) {
			$table->increments('id');
			$table->float('hour');
			$table->decimal('price', 12,4);
			$table->integer('worker_id')->unsigned();
			$table->integer('project_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('project_workers');
	}
}
