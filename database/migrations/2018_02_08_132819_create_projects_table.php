<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectsTable extends Migration {

	public function up()
	{
		Schema::create('projects', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 50);
			$table->text('description');
			$table->string('sdate');
			$table->string('edate');
			$table->float('budget', 20, 2);
			$table->float('fbudget', 20, 2)->nullable();
			$table->integer('status')->default('1')->comment('1:active,2:closes,3:finished');
			$table->integer('user_id')->unsigned();
			$table->integer('customer_id')->unsigned();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('projects');
	}
}
