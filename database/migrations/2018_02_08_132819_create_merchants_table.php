<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMerchantsTable extends Migration {

	public function up()
	{
		Schema::create('merchants', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 50);
			$table->text('address')->nullable();
			$table->string('phone', 20)->nullable();
			$table->integer('user_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('merchants');
	}
}