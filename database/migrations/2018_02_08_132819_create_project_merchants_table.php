<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectMerchantsTable extends Migration {

	public function up()
	{
		Schema::create('project_merchants', function(Blueprint $table) {
			$table->increments('id');
			$table->string('product');
			$table->float('price');
			$table->decimal('count');
			$table->string('attachment')->nullable();
			$table->integer('merchant_id')->unsigned();
			$table->integer('project_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('project_merchants');
	}
}