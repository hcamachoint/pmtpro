<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomersTable extends Migration {

	public function up()
	{
		Schema::create('customers', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 50);
			$table->string('address');
			$table->string('address2')->nullable();
			$table->string('zip', 10)->nullable();
			$table->string('city');
			$table->string('email')->nullable();
			$table->string('phone', 20)->nullable();
			$table->string('picture')->default('default-company.jpg');
			$table->integer('user_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('customers');
	}
}
