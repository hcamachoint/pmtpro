<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProjectBudgetsTable extends Migration {

	public function up()
	{
		Schema::create('project_budgets', function(Blueprint $table) {
			$table->increments('id');
			$table->text('description');
			$table->float('amount', 20, 2);
			$table->integer('project_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('project_budgets');
	}
}
