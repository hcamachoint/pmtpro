<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVendorsTable extends Migration {

	public function up()
	{
		Schema::create('vendors', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 50);
			$table->string('address')->nullable();
			$table->string('zip', 10)->nullable();
			$table->integer('city_id')->unsigned();
			$table->string('phone', 20)->nullable();
			$table->integer('user_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('vendors');
	}
}
