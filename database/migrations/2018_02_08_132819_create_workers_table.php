<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWorkersTable extends Migration {

	public function up()
	{
		Schema::create('workers', function(Blueprint $table) {
			$table->increments('id');
			$table->string('idn', 13)->nullable();
			$table->string('name', 50);
			$table->string('email', 200)->nullable();
			$table->string('phone', 20)->nullable();
			$table->string('jobtitle', 50)->nullable();
			$table->string('picture')->default('default.jpg');
			$table->string('address')->nullable();
			$table->string('zip', 10)->nullable();
			$table->string('city');
			$table->integer('user_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('workers');
	}
}
