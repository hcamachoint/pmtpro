@extends('layouts.app')

@section('content')
<div class="container">
  <!--{!!Form::open(['route'=>'project.store', 'files' => true, 'method' => 'post'])!!}
    <div id="projectc"></div>
  {!!Form::close()!!}-->
  <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <a href="{{ URL::previous() }}" class="btn btn-default">Go Back</a><br><br>
            <div class="panel panel-default">
                <div class="panel-heading"><h2>New Project</h2></div>
                {!!Form::open(['route'=>'project.store', 'files' => true, 'method' => 'post'])!!}
                  <div class="panel-body">
                      <h3>Customer
                        @if($cm != 0)<small><a type="button" data-toggle="modal" data-target="#createCustomer">New Customer</a></small>@endif
                      </h3>
                      @if($cm != 0)
                        {!!Form::select('customer_id', $customers, null, ['class' => 'form-control cusList form-sm', 'required'=>'true'])!!}
                      @else
                        <div class="col-md-3">
                          Customer Name <i class="required">*</i>
                        </div>
                        <div class="col-md-9">
                          {!!Form::text('cname', null, ['class'=>'form-control', 'placeholder'=>'Customer Name', 'required'=>'true', 'maxlength' => 50])!!}<br>
                        </div>
                        <div class="col-md-3">
                          Customer Address <i class="required">*</i>
                        </div>
                        <div class="col-md-9">
                          {!!Form::text('caddress', null, ['class'=>'form-control', 'placeholder'=>'Customer Address', 'maxlength' => 240, 'required'=>'true'])!!}<br>
                        </div>
                        <div class="col-md-3">
                          Customer City <i class="required">*</i>
                        </div>
                        <div class="col-md-9">
                          {!! Form::select('ccity', $cities, null,['class' => 'form-control', 'placeholder' => 'Select a city', 'maxlength' => 100, 'required'=>'true'])!!}<br>
                        </div>
                      @endif
                      <hr>
                      <h3>Project Budget and Description</h3><br>
                      <div class="row">
                        <div class="col-md-3">
                          Start date <i class="required">*</i>
                        </div>
                        <div class="col-md-9">
                          <input type='text' class="form-control form-sm datepicker" name="sdate" placeholder="Click to set"/><br>
                        </div>
                        <div class="col-md-3">
                          End date <i class="required">*</i>
                        </div>
                        <div class="col-md-9">
                          <input type='text' class="form-control form-sm datepicker" name="edate" placeholder="Click to set"/><br>
                        </div>
                        <div class="col-md-3">
                          Project Budget <i class="required">*</i>
                        </div>
                        <div class="col-md-9" style="margin-left:-6px">
                          <span class="input-dollar left">{!! Form::text('budget',null,['class' => 'form-control form-sm commifyble', 'maxlength' => 14, 'step'=>'any', 'required'=>'true', 'id' => 'budget', 'onkeyup' => 'originalBudget()', 'onchange' => 'decimable("budget")', 'placeholder' => 'Budget']) !!}</span><br>
                        </div>
                        <div class="col-md-3">
                          Project Name <i class="required">*</i>
                        </div>
                        <div class="col-md-9">
                          {!! Form::text('name',null,['class' => 'form-control form-bg', 'maxlength' => 50, 'required'=>'true', 'placeholder' => 'Project Name']) !!}<br>
                        </div>
                        <div class="col-md-3">
                          Project Description <i class="required">*</i>
                        </div>
                        <div class="col-md-9">
                          {!!Form::textarea('description', null, ['class'=>'form-control form-bg', 'rows' => 3, 'placeholder'=>'Project Description', 'required'=>'true'])!!}
                        </div>
                      </div>
                      <hr>
                      <h3>Materials</h3>
                          <table id="myTable" class="table order-list">
                          <thead>
                              <tr class="info">
                                <td>Description</td>
                                <td>Price</td>
                                <td>Quantity</td>
                                <td>Total</td>
                                <td></td>
                              </tr>
                          </thead>
                          <tbody>

                          </tbody>
                          <tfoot>
                            <tr>
                              <td>
                                  <input class="btn btn-md btn-block" type="button" id="addmat" value="New Row" />
                              </td>
                            </tr>
                          </tfoot>
                      </table>


                        <hr>
                        <h3>Worker <small><a type="button" data-toggle="modal" data-target="#createWorker">Add Worker</a></small></h3>
                        <table class="table order-list2">
                          <thead>
                            <tr class="success">
                              <td>Worker</td>
                              <td>Price</td>
                              <td>Rate</td>
                              <td>Total</td>
                              <td></td>
                            </tr>
                          </thead>
                          <tbody>

                          </tbody>
                          <tfoot>
                            <tr>
                              <td>
                                <div id="workerCreator" style="left">
                                  <input class="btn btn-md btn-block" type="button" id="addlab" value="New Row" />
                                </div>
                              </td>
                            </tr>
                          </tfoot>
                        </table>

                      <hr>
                      <button type="submit" name="button" class="btn btn-success">Save</button>
                      <div class="" align="center">
                        <h3>Finance</h3>
                        <b>Original Budget: </b>${!!Form::text('nprofit', '0.00', ['class'=>'reader commifyble form-n15 cooler', 'maxlength' => 10,  'id' => 'originBudget', 'readonly' => 'true'])!!}<br>
                        <b>Net Profit: </b>${!!Form::text('nprofit', '0.00', ['class'=>'reader commifyble form-n15 cooler', 'maxlength' => 10,  'id' => 'netProfit', 'readonly' => 'true'])!!}<br>
                        <b>Net Profit Margin: </b>%{!!Form::text('nprofit', '0.00', ['class'=>'reader commifyble form-n15 cooler', 'maxlength' => 10,  'id' => 'netProfitMargin', 'readonly' => 'true'])!!}<br>
                      </div>
                    </div>
                  {!!Form::close()!!}

                  <div class="modal fade" id="createWorker" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel" align="left">Create Worker</h4>
                        </div>
                        <div class="modal-body">
                          <p align="left">{!! Form::text('name',null,['class' => 'form-control', 'placeholder' => 'Name', 'maxlength' => 50, 'id' => 'labname', 'required'=>'true']) !!}</p>
                          <p align="left">{!! Form::text('address',null,['class' => 'form-control', 'placeholder' => 'Address', 'id' => 'labaddress', 'required'=>'true']) !!}</p>
                          <p align="left">{!! Form::select('city', $cities, null,['class' => 'form-control', 'placeholder' => 'Select a city', 'id' => 'labcity', 'required'=>'true']) !!}</p>
                        </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">I'm not sure</button>
                            <button type="submit" name="button" class="btn btn-success" onclick="workerCreate()">Save</button>
                          </div>
                      </div>
                    </div>
                  </div>

                  <div class="modal fade" id="createCustomer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel" align="left">Create Customer</h4>
                        </div>
                        <div class="modal-body">
                          <p align="left">{!! Form::text('name',null,['class' => 'form-control', 'placeholder' => 'Name', 'maxlength' => 50, 'id' => 'cusname', 'required'=>'true']) !!}</p>
                          <p align="left">{!! Form::text('address',null,['class' => 'form-control', 'placeholder' => 'Address', 'id' => 'cusaddress', 'required'=>'true']) !!}</p>
                          <p align="left">{!! Form::select('city', $cities, null,['class' => 'form-control', 'placeholder' => 'Select a city', 'id' => 'cuscity', 'required'=>'true']) !!}</p>
                        </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">I'm not sure</button>
                            <button type="submit" name="button" class="btn btn-success" onclick="customerCreate()">Save</button>
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
                <a href="{{ URL::previous() }}" class="btn btn-default">Go Back</a>
            </div>
        </div>
    </div>
</div>
<div id="sel" style="visibility: hidden">
  {!!Form::select('worker_id[]', $workers, null, ['class' => 'form-control labList', 'placeholder' => 'Select a Worker', 'required'=>'true'])!!}
</div>
<div style="visibility: hidden">
  {!! Form::text('pm',0,['id' => 'pm']) !!}
  {!! Form::text('pl',0,['id' => 'pl']) !!}
</div>
@endsection

@section('footer')
<script src="{{ asset('js/timepicker.js') }}"></script>
<script src="{{ asset('js/commif.js') }}"></script>
<script src="{{ asset('js/projectc.js') }}"></script>
@endsection
