@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <!--PROJECT-->
            <div class="panel panel-default">
                <div class="panel-heading"><h2>{{$project->name}}</h2></div>
                  <div class="panel-body">
                      <h3>Customer </h3>{{$project->customers->name}}
                      <hr>
                      <h3>Project Budget and Description</h3><br>
                      <div class="row">
                        <div class="col-md-12">
                          <b>Start date:</b> {{\Carbon\Carbon::parse($project->sdate)->format('Y-m-d')}}
                        </div>
                        <div class="col-md-12">
                          <b>End date: </b>{{\Carbon\Carbon::parse($project->edate)->format('Y-m-d')}}
                        </div>
                        <div class="col-md-12">
                          <b>Project Budget:</b>${!! Form::text('budget',$project->budget,['class' => 'reader commifyble','step'=>'any', 'readonly'=>'true', 'id' => 'budget']) !!}
                        </div>
                        <div class="col-md-12">
                          <b>Project Name:</b> {{$project->name}}
                        </div>
                        <div class="col-md-12">
                          <b>Project Description:</b> {{$project->description}}
                        </div>
                      </div>
                    </div>
                  </div>

                  <!--MATERIALS-->
                  @if (count($project->materials) !== 0)
                    <div class="panel panel-default">
                      {!!Form::open(['route'=> ['projectmaterial.update', $project->id], 'files' => true, 'method' => 'put'])!!}
                      <div class="panel-body">
                        <h3>Materials</h3>
                            <table id="myTable" class="table order-list">
                            <thead>
                                <tr class="info">
                                  <!--<td>Store</td>-->
                                  <td>Description</td>
                                  <!--<td>Attachment</td>-->
                                  <td>Price</td>
                                  <td>Quantity</td>
                                  <td>Total</td>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($projectMaterials as $material)
                                <tr>
                                  <td><input type="text" class="form-control" name="material_description[]{{$loop->iteration}}" value="{{$material->description}}" required disabled/></td>
                                  <!--<td><input type="file" name="material_attachment[]{{$loop->iteration}}"></td>-->
                                  <td><span class="input-dollar left"><input type=number class="form-control" name="material_price[]{{$loop->iteration}}" id="material_price_{{$loop->iteration}}" onkeyup="materialCalc({{$loop->iteration}})" step=any value="{{number_format($material->price, 2)}}" required disabled/></span></td>
                                  <td><input type="number" class="form-control" name="material_count[]{{$loop->iteration}}" id="material_count_{{$loop->iteration}}" onkeyup="materialCalc({{$loop->iteration}})" step=any value="{{number_format($material->count, 2)}}" required disabled/></td>
                                  <td><input class="calculable form-control" id="totmat_{{$loop->iteration}}" value="{{number_format($material->price * $material->count, 2)}}" disabled></td>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                      </div>
                      {!!Form::close()!!}
                    </div>
                  @endif

                  @if (count($project->workers) !== 0)
                    <!--WorkerS-->
                    <div class="panel panel-default">
                      {!!Form::open(['route'=> ['projectworker.update', $project->id], 'files' => false, 'method' => 'put'])!!}
                      <div class="panel-body">
                          <h3>Worker <small><a type="button" data-toggle="modal" data-target="#createWorker">New Worker</a></small></h3>
                          <table class="table order-list2">
                            <thead>
                              <tr class="success">
                                <td>Worker</td>
                                <td>Price</td>
                                <td>Rate</td>
                                <td>Total</td>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($projectWorkers as $worker)
                                <tr>
                                  <td>{!!Form::select('worker_id[]', $workers, $worker->worker_id, ['class' => 'form-control', 'disabled'=>'true'])!!}</td>
                                  <td><span class="input-dollar left"><input id="worker_price_{{$loop->iteration}}" onkeyup="workerCalc({{$loop->iteration}})" type="number" class="form-control" name="worker_price[]{{$loop->iteration}}" value="{{number_format($worker->price, 2)}}" required disabled/></span></td>
                                  <td><input id="worker_hours_{{$loop->iteration}}" onkeyup="workerCalc({{$loop->iteration}})" type="number" step=any class="form-control" name="worker_hours[]{{$loop->iteration}}" value="{{number_format($worker->hour, 2)}}" required disabled/></td>
                                  <td><input class="calculable form-control" id="totlab_{{$loop->iteration}}" value="{{number_format($worker->price * $worker->hour, 2)}}" disabled></td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                        {!!Form::close()!!}
                      </div>
                    @endif

                  <!--FINANCES-->
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <div align="right">
                        <h3>Finance</h3>
                        <b>Original Budget: </b>${!!Form::text('nprofit', '0.00', ['class'=>'reader commifyble form-n6 cooler', 'maxlength' => 10,  'id' => 'originBudget', 'readonly' => 'true'])!!}<br>
                        <b>Net Profit: </b>${!!Form::text('nprofit', '0.00', ['class'=>'reader commifyble form-n6 cooler', 'maxlength' => 10,  'id' => 'netProfit', 'readonly' => 'true'])!!}<br>
                        <b>Net Profit Margin: </b>%{!!Form::text('nprofit', '0.00', ['class'=>'reader commifyble form-n6 cooler', 'maxlength' => 10,  'id' => 'netProfitMargin', 'readonly' => 'true'])!!}<br>
                      </div>
                    </div>
                    <div align="right" style="margin-right:15px; margin-left:15px">
                      <!--Project Options-->
                      <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Project Options
                      </button>
                      <div class="collapse" id="collapseExample">
                        <br>
                        <!--Project Options content-->
                        <div class="well">
                          <button type="button" class="btn btn-default">
                            PDF Report
                          </button>
                          @if($project->status == 1)
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#closeProject">
                              Close project
                            </button>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#finishProject">
                              Finish project
                            </button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteProject">
                              Delete project
                            </button>
                          @else
                          <button type="button" class="btn btn-default" data-toggle="modal" data-target="#openProject">
                            Open project
                          </button>
                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteProject">
                            Delete project
                          </button>
                          @endif
                          <!-- Delete Project -->
                          <div class="modal fade" id="deleteProject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel" align="left">{{$project->name}}</h4>
                                </div>
                                <div class="modal-body">
                                  <p align="left">Are you sure you want to eliminate this project?</p>
                                  <p align="left">Once the project is eliminated there will be no way to reverse the changes.</p>
                                </div>
                                {!!Form::open(['route'=>['project.destroy', $project->id],'method' => 'delete'])!!}
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">I'm not sure</button>
                                    <button type="submit" class="btn btn-danger">Yes, delete</button>
                                  </div>
                                {!!Form::close()!!}
                              </div>
                            </div>
                          </div>
                          <!--Delete Project-->

                          <!-- Finish Project -->
                          <div class="modal fade" id="finishProject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel" align="left">{{$project->name}}</h4>
                                </div>
                                <div class="modal-body">
                                  <p align="left">Are you sure you want to finish this project?</p>
                                  <p align="left"></p>
                                </div>
                                {!!Form::open(['route'=>['project.finish', $project->id],'method' => 'put'])!!}
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">I'm not sure</button>
                                    <button type="submit" class="btn btn-success">Yes, finish</button>
                                  </div>
                                {!!Form::close()!!}
                              </div>
                            </div>
                          </div>
                          <!--Finish Project-->

                          <!-- Close Project -->
                          <div class="modal fade" id="closeProject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel" align="left">{{$project->name}}</h4>
                                </div>
                                <div class="modal-body">
                                  <p align="left">Are you sure you want to close this project?</p>
                                  <p align="left">Once the project is close there will be no way to reverse the changes.</p>
                                </div>
                                {!!Form::open(['route'=>['project.close', $project->id],'method' => 'put'])!!}
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">I'm not sure</button>
                                    <button type="submit" class="btn btn-primary">Yes, close</button>
                                  </div>
                                {!!Form::close()!!}
                              </div>
                            </div>
                          </div>
                          <!--Close Project-->

                          <!-- Open Project -->
                          <div class="modal fade" id="openProject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel" align="left">{{$project->name}}</h4>
                                </div>
                                <div class="modal-body">
                                  <p align="left">Are you sure you want to close this project?</p>
                                  <p align="left">Once the project is close there will be no way to reverse the changes.</p>
                                </div>
                                {!!Form::open(['route'=>['project.open', $project->id],'method' => 'put'])!!}
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">I'm not sure</button>
                                    <button type="submit" class="btn btn-primary">Yes, open</button>
                                  </div>
                                {!!Form::close()!!}
                              </div>
                            </div>
                          </div>
                          <!--Open Project-->
                        </div>
                      </div>
                    </div>
                  <br>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="sel" style="visibility: hidden">
  {!!Form::select('worker_id[]', $workers, null, ['class' => 'form-control labList', 'required'=>'true'])!!}
</div>
<div style="visibility: hidden">
  {!! Form::text('pm',$pm,['id' => 'pm']) !!}
  {!! Form::text('pl',$pl,['id' => 'pl']) !!}
</div>
@endsection

@section('footer')
<script src="{{ asset('js/timepicker.js') }}"></script>
<script src="{{ asset('js/commif.js') }}"></script>
<script src="{{ asset('js/projectc.js') }}"></script>
@endsection
