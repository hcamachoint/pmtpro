@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="well" style="background-color: white">
			<div class="container">
				<h1>Project List <a href="{{ route('project.create') }}" class="btn btn-primary">New</a></h1>
				<a href="{{ route('project.old') }}">Completed Projects</a>
			</div>
		</div>
		@if (count($projects) !== 0)
			<div class="well" style="background-color: white">
				<table class="table table-hover">
					<thead>
						<th>Name</th>
						<th>Budget</th>
						<th>Status</th>
					</thead>
					<tbody>
						@foreach($projects as $project)
							<tr>
								<td>{!! link_to_route('project.edit', $project->name, [$project->id]) !!}</td>
								<td>${!!Form::text('budget', $project->budget, ['class'=>'reader commifyble', 'readonly' => 'true'])!!}</td>
								<td>Active</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		@else
			<div class="well" style="background-color: white">
			    <div class="page-header">
					<h1>You don't have projects registered</h1>
				</div>
				<div>
					<p>Create a new project.</p>
				</div>
			</div>
		@endif
	</div>
@endsection

@section('footer')
<script src="{{ asset('js/commif.js') }}"></script>
@endsection
