@extends('layouts.app')

@section('content')
<div class="container">
  <!--{!!Form::open(['route'=>'project.store', 'files' => true, 'method' => 'post'])!!}
    <div id="projectc"></div>
  {!!Form::close()!!}-->
  <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <a href="{{ URL::previous() }}" class="btn btn-default">Go Back</a><br><br>
            <div class="panel panel-default">
                <div class="panel-heading"><h2>{{$project->name}}</h2></div>
                {!!Form::open(['route'=> ['project.update', $project->id], 'files' => true, 'method' => 'put'])!!}
                  <div class="panel-body">
                    <h3>Customer
                      <small><a type="button" data-toggle="modal" data-target="#createCustomer">New Customer</a></small>
                    </h3>
                    {!!Form::select('customer_id', $customers, $project->customer_id, ['class' => 'form-control cusList form-sm'])!!}
                    <hr>

                      <hr>
                      <h3>Project Budget and Description</h3><br>
                      <div class="row">
                        <div class="col-md-3">
                          Start date <i class="required">*</i>
                        </div>
                        <div class="col-md-9 form-group{{ $errors->has('sdate') ? ' has-error' : '' }}">
                          <input type='text' class="form-control form-sm datepicker" name="sdate" value="{{$project->sdate}}" placeholder="Click to set"/>
                          @if ($errors->has('sdate'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('sdate') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="col-md-3">
                          End date <i class="required">*</i>
                        </div>
                        <div class="col-md-9 form-group{{ $errors->has('edate') ? ' has-error' : '' }}">
                          <input type='text' class="form-control form-sm datepicker" name="edate" value="{{$project->edate}}" placeholder="Click to set"/>
                          @if ($errors->has('edate'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('edate') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="col-md-3">
                          Project Budget <i class="required">*</i>
                        </div>
                        <div class="col-md-9 form-group{{ $errors->has('budget') ? ' has-error' : '' }}" style="margin-left:-6px">
                          <span class="input-dollar left">{!! Form::text('budget',$project->budget,['class' => 'form-control form-sm commifyble', 'maxlength' => 14, 'step'=>'any', 'required'=>'true', 'id' => 'budget', 'onkeyup' => 'originalBudget()', 'onchange' => 'decimable("budget")', 'placeholder' => 'Budget']) !!}</span>
                          @if ($errors->has('budget'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('budget') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="col-md-3">
                          Project Name <i class="required">*</i>
                        </div>
                        <div class="col-md-9 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          {!! Form::text('name',$project->name,['class' => 'form-control form-bg', 'required'=>'true', 'placeholder' => 'Project Name']) !!}
                          @if ($errors->has('name'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('name') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="col-md-3">
                          Project Description <i class="required">*</i>
                        </div>
                        <div class="col-md-9 form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                          {!!Form::textarea('description', $project->description, ['class'=>'form-control form-bg', 'rows' => 3, 'placeholder'=>'Project Description', 'required'=>'true'])!!}
                          @if ($errors->has('description'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('description') }}</strong>
                              </span>
                          @endif
                        </div>
                        <div class="col-md-3">
                          <button type="submit" name="button" class="btn btn-success">Save</button>
                        </div>
                      </div>
                      {!!Form::close()!!}
                    </div>
                </div>

                <!--MATERIALS-->
                <div class="panel panel-default">
                  {!!Form::open(['route'=> ['projectmaterial.update', $project->id], 'files' => true, 'method' => 'put'])!!}
                  <div class="panel-body">
                      <h3>Materials</h3>
                          <table id="myTable" class="table order-list">
                          <thead>
                              <tr class="info">
                                <td>Description</td>
                                <td>Price</td>
                                <td>Quantity</td>
                                <td>Total</td>
                                <td></td>
                              </tr>
                          </thead>
                          <tbody>
                            @foreach($projectMaterials as $material)
                              <tr>
                                <td><input type="text" class="form-control" name="material_description[]{{$loop->iteration}}" value="{{$material->description}}" required=""></td>
                                <td><span class="input-dollar left"><input id="material_price_{{$loop->iteration}}" onkeyup="materialCalc({{$loop->iteration}})" class="form-control materialprice" name="material_price[]{{$loop->iteration}}" type="text" step="any" value="{{number_format($material->price, 2)}}" required=""></span></td>
                                <td><input id="material_count_{{$loop->iteration}}" onkeyup="materialCalc({{$loop->iteration}})" type="number" step="any" class="form-control" name="material_count[]{{$loop->iteration}}" value="{{number_format($material->count, 2)}}" required=""></td>
                                <td><input class="calculable form-control" id="totmat_{{$loop->iteration}}" value="{{number_format($material->price * $material->count, 2)}}" disabled=""></td>
                                <td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>
                              </tr>
                            @endforeach
                          </tbody>
                          <tfoot>
                            <tr>
                              <td>
                                  <input class="btn btn-md btn-block" type="button" id="addmat" value="New Row" />
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <button type="submit" name="button" class="btn btn-success">Save</button>
                              </td>
                            </tr>
                          </tfoot>
                      </table>
                    </div>
                    {!!Form::close()!!}
                  </div>

                  <!--WORKERS-->
                  <div class="panel panel-default">
                    {!!Form::open(['route'=> ['projectworker.update', $project->id], 'files' => false, 'method' => 'put'])!!}
                    <div class="panel-body">
                        <h3>Worker <small><a type="button" data-toggle="modal" data-target="#createWorker">Add Worker</a></small></h3>
                        <table class="table order-list2">
                          <thead>
                            <tr class="success">
                              <td>Worker</td>
                              <td>Price</td>
                              <td>Rate</td>
                              <td>Total</td>
                              <td></td>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($projectWorkers as $worker)
                              <tr>
                                <td>{!!Form::select('worker_id[]', $workers, $worker->worker_id, ['class' => 'form-control', 'required'=>'true'])!!}</td>
                                <td><span class="input-dollar left"><input id="worker_price_{{$loop->iteration}}" onkeyup="workerCalc({{$loop->iteration}})" type="text" class="form-control commifyble" name="worker_price[]{{$loop->iteration}}" value="{{$worker->price}}" required/></span></td>
                                <td><input id="worker_hours_{{$loop->iteration}}" onkeyup="workerCalc({{$loop->iteration}})" type="text" step=any class="form-control" name="worker_hours[]{{$loop->iteration}}" value="{{number_format($worker->hour, 2)}}" required/></td>
                                <td><input class="calculable commifyble form-control"  id="totlab_{{$loop->iteration}}" value="{{number_format($worker->price * $worker->hour, 2)}}" disabled></td>
                                <td><input type="button" class="ibtnDel btn btn-md btn-danger "  value="Delete"></td>
                              </tr>
                            @endforeach
                          </tbody>
                          <tfoot>
                            <tr>
                              <td>
                                <div id="workerCreator" style="left">
                                  <input class="btn btn-md btn-block" type="button" id="addlab" value="New Row" />
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <button type="submit" name="button" class="btn btn-success">Save</button>
                              </td>
                            </tr>
                          </tfoot>
                        </table>
                      {!!Form::close()!!}
                      <hr>
                      <div align="center">
                        <h3>Finance</h3>
                        <b>Original Budget: </b>${!!Form::text('nprofit', '0.00', ['class'=>'reader commifyble form-n15 cooler', 'maxlength' => 10,  'id' => 'originBudget', 'readonly' => 'true'])!!}<br>
                        <b>Net Profit: </b>${!!Form::text('nprofit', '0.00', ['class'=>'reader commifyble form-n15 cooler', 'maxlength' => 10,  'id' => 'netProfit', 'readonly' => 'true'])!!}<br>
                        <b>Net Profit Margin: </b>%{!!Form::text('nprofit', '0.00', ['class'=>'reader commifyble form-n15 cooler', 'maxlength' => 10,  'id' => 'netProfitMargin', 'readonly' => 'true'])!!}<br>
                      </div>
                    </div>
                  <div align="right" style="margin-right:15px; margin-left:15px">
                      <!--Project Options-->
                      <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample" style="margin-bottom: 25px">
                        Project Options
                      </button>
                      <div class="collapse" id="collapseExample">
                        <br>
                        <!--Project Options content-->
                        <div class="well">
                          <a type="button" class="btn btn-default" href="{{ route('project.pdf', $project->id) }}" target="_blank">
                            PDF Report
                          </a>
                          @if($project->status == 1)
                            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#closeProject">
                              Close project
                            </button>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#finishProject">
                              Finish project
                            </button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteProject">
                              Delete project
                            </button>
                          @elseif($project->status == 2)
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#openProject">
                              Open project
                            </button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteProject">
                              Delete project
                            </button>
                          @else
                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteProject">
                            Delete project
                          </button>
                          @endif
                          <!-- Delete Project -->
                          <div class="modal fade" id="deleteProject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel" align="left">{{$project->name}}</h4>
                                </div>
                                <div class="modal-body">
                                  <p align="left">Are you sure you want to eliminate this project?</p>
                                  <p align="left">Once the project is eliminated there will be no way to reverse the changes.</p>
                                </div>
                                {!!Form::open(['route'=>['project.destroy', $project->id],'method' => 'delete'])!!}
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">I'm not sure</button>
                                    <button type="submit" class="btn btn-danger">Yes, delete</button>
                                  </div>
                                {!!Form::close()!!}
                              </div>
                            </div>
                          </div>
                          <!--Delete Project-->

                          <!-- Finish Project -->
                          <div class="modal fade" id="finishProject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel" align="left">{{$project->name}}</h4>
                                </div>
                                <div class="modal-body">
                                  <p align="left">Are you sure you want to finish this project?</p>
                                  <p align="left"></p>
                                </div>
                                {!!Form::open(['route'=>['project.finish', $project->id],'method' => 'put'])!!}
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">I'm not sure</button>
                                    <button type="submit" class="btn btn-success">Yes, finish</button>
                                  </div>
                                {!!Form::close()!!}
                              </div>
                            </div>
                          </div>
                          <!--Finish Project-->

                          <!-- Close Project -->
                          <div class="modal fade" id="closeProject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel" align="left">{{$project->name}}</h4>
                                </div>
                                <div class="modal-body">
                                  <p align="left">Are you sure you want to close this project?</p>
                                  <p align="left">Once the project is close there will be no way to reverse the changes.</p>
                                </div>
                                {!!Form::open(['route'=>['project.close', $project->id],'method' => 'put'])!!}
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">I'm not sure</button>
                                    <button type="submit" class="btn btn-warning">Yes, close</button>
                                  </div>
                                {!!Form::close()!!}
                              </div>
                            </div>
                          </div>
                          <!--Close Project-->

                          <!-- Open Project -->
                          <div class="modal fade" id="openProject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel" align="left">{{$project->name}}</h4>
                                </div>
                                <div class="modal-body">
                                  <p align="left">Are you sure you want to close this project?</p>
                                  <p align="left">Once the project is close there will be no way to reverse the changes.</p>
                                </div>
                                {!!Form::open(['route'=>['project.open', $project->id],'method' => 'put'])!!}
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">I'm not sure</button>
                                    <button type="submit" class="btn btn-primary">Yes, open</button>
                                  </div>
                                {!!Form::close()!!}
                              </div>
                            </div>
                          </div>
                          <!--Open Project-->
                        </div>
                      </div>

                  <div class="modal fade" id="createWorker" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel" align="left">Create Worker</h4>
                        </div>
                        <div class="modal-body">
                          <p align="left">{!! Form::text('name',null,['class' => 'form-control', 'placeholder' => 'Name', 'maxlength' => 50, 'id' => 'labname', 'required'=>'true']) !!}</p>
                          <p align="left">{!! Form::text('address',null,['class' => 'form-control', 'placeholder' => 'Address', 'id' => 'labaddress', 'required'=>'true']) !!}</p>
                          <p align="left">{!! Form::select('city', $cities, null,['class' => 'form-control', 'placeholder' => 'Select a city', 'id' => 'labcity', 'required'=>'true']) !!}</p>
                        </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">I'm not sure</button>
                            <button type="submit" name="button" class="btn btn-success" onclick="WorkerCreate()">Save</button>
                          </div>
                      </div>
                    </div>
                  </div>

                </div>

            </div>
            <a href="{{ URL::previous() }}" class="btn btn-default">Go Back</a>
        </div>
    </div>
</div>
<div id="sel" style="visibility: hidden">
  {!!Form::select('worker_id[]', $workers, null, ['class' => 'form-control labList', 'placeholder' => 'Select a Worker', 'required'=>'true'])!!}
</div>
<div style="visibility: hidden">
  {!! Form::text('pm',0,['id' => 'pm']) !!}
  {!! Form::text('pl',0,['id' => 'pl']) !!}
</div>
@endsection

@section('footer')
<script src="{{ asset('js/timepicker.js') }}"></script>
<script src="{{ asset('js/commif.js') }}"></script>
<script src="{{ asset('js/projectc.js') }}"></script>
@endsection
