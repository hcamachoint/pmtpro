@extends('layouts.app')

@section('content')

	<div class="container">
    <div class="well" style="background-color: white">
      <div class="container">
        <h1>Completed/Closed Projects</h1>
				<a href="{{ route('project.index') }}">Project List</a>
      </div>
    </div>
		@if (count($projects) !== 0)
			<div class="well" style="background-color: white">
				<table class="table table-hover">
					<thead>
						<th>Name</th>
						<th>Budget</th>
						<th>Status</th>
					</thead>
					<tbody>
						@foreach($projects as $project)
							<tr>
								<td>{!! link_to_route('project.show', $project->name, [$project->id]) !!}</td>
								<td>${!!Form::text('budget', $project->budget, ['class'=>'reader commifyble', 'readonly' => 'true'])!!}</td>
								<td>
									@if($project->status == 2)
										Closed
									@else
										Finished
									@endif
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		@else
			<div class="well" style="background-color: white">
			    <h3>You don't have completed projects</h3>
			</div>
		@endif
	</div>
@endsection

@section('footer')
<script src="{{ asset('js/cleave.js') }}"></script>
<script src="{{ asset('js/commif.js') }}"></script>
@endsection
