@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h2>New Project</h2></div>

                <div class="panel-body">
                    <select>
                      <option>Customer 1</option>
                      <option>Customer 2</option>
                      <option>Customer 3</option>
                    </select>
                    <hr>
                    <img class="img-responsive img-circle" src="/company.jpg" style="width:85px; height:85px;">
                    <h4>Customer 1 Company Name</h4>
                    <p>Customer 1 address one</p>
                    <p>Customer 1 address two</p>
                    <p>Customer 1 email</p>
                    <hr>
                    <h3>Project Budget and Description</h3>
                    <div class="row">
                      <div class="col-md-3">
                        <p>Start date</p>
                      </div>
                      <div class="col-md-9">
                        <input type="text" name="budget" value="" placeholder="01/01/2018" class="form-control">
                      </div>
                      <div class="col-md-3">
                        <p>Completation date</p>
                      </div>
                      <div class="col-md-9">
                        <input type="text" name="budget" value="" placeholder="31/12/2018" class="form-control">
                      </div>
                      <div class="col-md-3">
                        <p>Project Budget</p>
                      </div>
                      <div class="col-md-9">
                        <input type="text" name="budget" value="" placeholder="Budget amount" class="form-control">
                      </div>
                      <div class="col-md-3">
                        <p>Project Added Budget</p>
                      </div>
                      <div class="col-md-9">
                        <input type="text" name="budget" value="" placeholder="Budget amount" class="form-control">
                      </div>
                      <div class="col-md-3">
                        <p>Project Description</p>
                      </div>
                      <div class="col-md-9">
                        <textarea name="name" rows="4" cols="71"></textarea>
                      </div>
                    </div>
                    <hr>
                    <h3>Materials</h3>
                      <table class="table table-responsive">
                        <tr class="info">
                          <td>Store</td>
                          <td>Description</td>
                          <td>Attachment</td>
                          <td>Price</td>
                          <td>Count</td>
                          <td>Total</td>
                        </tr>
                        <tr>
                          <td>
                            <select class="form-control">
                              <option>Product</option>
                              <option>Product</option>
                              <option>Product</option>
                            </select>
                          </td>
                          <td>Home Depot Purchase of materials</td>
                          <td><input type="file" name="" value=""></td>
                          <td>69.33</td>
                          <td>1</td>
                          <td>69.33</td>
                        </tr>
                        <tr>
                          <td>
                            <select class="form-control">
                              <option>Product</option>
                              <option>Product</option>
                              <option>Product</option>
                            </select>
                          </td>
                          <td>Lowes Materials</td>
                          <td><input type="file" name="" value=""></td>
                          <td>50.33</td>
                          <td>2</td>
                          <td>100.66</td>
                        </tr>
                      </table>
                      <button type="button" name="button" class="btn btn-default">Add row</button>
                      <hr>
                      <h3>Worker</h3>
                      <table class="table table-responsive">
                        <tr class="success">
                          <td>Worker</td>
                          <td>Name</td>
                          <td>Price hrs</td>
                          <td>Hours</td>
                          <td>Total</td>
                        </tr>
                        <tr>
                          <td>
                            <select class="form-control">
                              <option>Service</option>
                              <option>Service</option>
                              <option>Service</option>
                            </select>
                          </td>
                          <td>Pedro Perez</td>
                          <td>20.00</td>
                          <td>8</td>
                          <td>160.00</td>
                        </tr>
                        <tr>
                          <td>
                            <select class="form-control">
                              <option>Service</option>
                              <option>Service</option>
                              <option>Service</option>
                            </select>
                          </td>
                          <td>Juan Ortega</td>
                          <td>10.00</td>
                          <td>8</td>
                          <td>80.00</td>
                        </tr>
                      </table>
                      <button type="button" name="button" class="btn btn-default">Add row</button>
                    <hr>
                    <button type="button" name="button" class="btn btn-success">Save</button>
                    <div class="" align="right">
                      <h3>Finance</h3>
                      <b>Original Budget: </b>0.00 $<br>
                      <b>Net Profit: </b>0.00 $<br>
                      <b>Net Profit Margin: </b>0.00 %<br>
                      <b>Total: </b>0.00 $<br>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
