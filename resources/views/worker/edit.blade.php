@extends('layouts.app')

@section('content')
	<div class="container">
		<a href="{{ URL::previous() }}" class="btn btn-default">Go Back</a><br><br>
		<div class="well" style="background-color: white">
		    <div class="page-header">
				<h1>{{$worker->name}}</h1>
			</div>
			<div class="container-fluid">
				{!!Form::open(['route'=>['worker.update', $worker->id], 'method' => 'put'])!!}

					{!!Form::label('State ID')!!}
					{!!Form::text('idn', $worker->idn, ['class'=>'form-control', 'placeholder'=>'Example: A123456789012', 'maxlength' => 13])!!}<br>

	        {!!Form::label('Name')!!} <i class="required">*</i>
	        {!!Form::text('name', $worker->name, ['class'=>'form-control', 'placeholder'=>'Name', 'required'=>'true', 'maxlength' => 50])!!}<br>

					{!!Form::label('Job Title')!!}
	        {!!Form::text('jobtitle', $worker->jobtitle, ['class'=>'form-control', 'placeholder'=>'Job Title', 'maxlength' => 50])!!}<br>

					{!!Form::label('Email')!!}
					{!!Form::text('email', $worker->email, ['class'=>'form-control', 'placeholder'=>'Email'])!!}<br>

					{!!Form::label('Phone')!!}
	        {!!Form::text('phone', $worker->phone, ['class'=>'form-control', 'placeholder'=>'Phone', 'maxlength' => 20])!!}<br>

	        {!!Form::label('Address')!!} <i class="required">*</i>
					{!!Form::textarea('address', $worker->address, ['class'=>'form-control', 'placeholder'=>'Address', 'required'=>'true'])!!}<br>

					{!!Form::label('Zip Code')!!}
					{!!Form::text('zip', $worker->zip, ['class'=>'form-control', 'placeholder'=>'Zip Code'])!!}<br>

					{!!Form::label('City')!!} <i class="required">*</i>
					{!!Form::select('city', $cities, $worker->city, ['class' => 'form-control'])!!}<br>

					{!!Form::button('Save', ['class'=>'btn btn-success','type'=>'submit'])!!}
					<!--<a type="button" data-toggle="modal" data-target="#addDocument" class="btn btn-primary">Add Document</a>-->
					<!--Create Customer Modal
					<div class="modal fade" id="addDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="myModalLabel" align="left">Create Customer</h4>
								</div>
								<div class="modal-body">
									<p align="left">Expiration Date: {!!Form::date('sdate', \Carbon\Carbon::now(), null, ['class' => 'form-control'])!!}</p>
									<p><input name="material_attachment[]" type="file" class="form-control"></p>
								</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">I'm not sure</button>
										<button type="submit" name="button" class="btn btn-success" onclick="customerCreate()">Save</button>
									</div>
							</div>
						</div>
					</div>-->
					<!--Create Customer Modal-->

				{!!Form::close()!!}

				<br>

				{!!Form::open(['route'=> ['worker.destroy', $worker->id], 'method' => 'delete'])!!}
						{!!Form::button('Delete ', ['type'=>'submit', 'class' => 'btn btn-danger'])!!}
				{!!Form::close()!!}
			</div>
		</div>
		<a href="{{ URL::previous() }}" class="btn btn-default">Go Back</a><br><br>
	</div>
@endsection
