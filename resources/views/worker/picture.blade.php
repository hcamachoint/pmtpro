@extends('layouts.app')

@section('content')

<div class="container">
	<div class="well" style="background-color: white">
			<div class="page-header">
				<h3>Worker: <small>{{ $worker->name }}</small></h3>
			</div>
			<div class="container">
				<img src="/uploads/workers/{{ $worker->picture }}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">
				<form enctype="multipart/form-data" action="/worker/picture/{{$worker->id}}" method="POST">
						<label>Update Worker Image</label>
						<input type="file" name="picture">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="submit" class="btn btn-sm btn-primary" value="Save">
				</form>
			</div>
		</div>
		<a href="{{ URL::previous() }}" class="btn btn-default">Go Back</a>
</div>

@endsection
