@extends('layouts.app')

@section('content')
	<div class="container">
		<a href="{{ URL::previous() }}" class="btn btn-default">Go Back</a><br><br>
		<div class="well" style="background-color: white">
      <div class="page-header"><h2>New Worker</h2></div>
			{!!Form::open(['route'=>'worker.store', 'method' => 'post'])!!}

				{!!Form::label('State ID')!!}
				{!!Form::text('idn', null, ['class'=>'form-control', 'placeholder'=>'Example: A123456789012', 'maxlength' => 13])!!}<br>

        {!!Form::label('Name')!!} <i class="required">*</i>
        {!!Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Name', 'required'=>'true', 'maxlength' => 50])!!}<br>

				{!!Form::label('Job Title')!!}
        {!!Form::text('jobtitle', null, ['class'=>'form-control', 'placeholder'=>'Job Title', 'maxlength' => 50])!!}<br>

				{!!Form::label('Email')!!}
				{!!Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Email'])!!}<br>

        {!!Form::label('Phone')!!}
        {!!Form::text('phone', null, ['class'=>'form-control', 'placeholder'=>'Phone', 'maxlength' => 20])!!}<br>

				{!!Form::label('Address')!!} <i class="required">*</i>
				{!!Form::textarea('address', null, ['class'=>'form-control', 'placeholder'=>'Address', 'required'=>'true'])!!}<br>

				{!!Form::label('Zip Code')!!}
				{!!Form::text('zip', null, ['class'=>'form-control', 'placeholder'=>'Zip Code'])!!}<br>

				{!!Form::label('City')!!} <i class="required">*</i>
				{!!Form::select('city', $cities, null, ['class' => 'form-control', 'placeholder' => 'Select a City', 'required' => 'true'])!!}<br>

				{!!Form::button('Save', ['class'=>'btn btn-success','type'=>'submit'])!!}

			{!!Form::close()!!}
		</div>
		<a href="{{ URL::previous() }}" class="btn btn-default">Go Back</a><br><br>
	</div>
@endsection
