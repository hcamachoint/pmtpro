@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="well" style="background-color: white">
			<div class="container">
				<h4>Add new Worker <a href="{{ route('worker.create') }}" class="btn btn-primary">Here</a></h4>
			</div>
		</div>
		@if (count($workers) !== 0)
			<div class="well" style="background-color: white">
				<table class="table table-hover">
					<thead>
						<th>Photo</th>
						<th>Name</th>
						<th>Phone</th>
						<th>Address</th>
						<th>Action</th>
					</thead>
					<tbody>
						@foreach($workers as $worker)
							<tr>
								<td><img class="img-responsive img-circle" src="/uploads/workers/{{ $worker->picture }}" style="width:45px; height:45px;"></td>
								<td><a href="#">{!! link_to_route('worker.edit', $worker->name, [$worker->id]) !!}</a></td>
								<td>{{$worker->phone}}</td>
								<td>{{$worker->address}}</td>
								<td><a href="#">{!! link_to_route('worker.picture', 'Set Photo', [$worker->id]) !!}</a></td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		@else
			<div class="well" style="background-color: white">
			    <div class="page-header">
					<h1>You don't have Workers registered</h1>
				</div>
				<div>
					<p>Create a new Worker.</p>
				</div>
			</div>
		@endif
	</div>
@endsection
