@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="well" style="background-color: white">
			<div class="container">
				<h4>Add new Vendor <a href="{{ route('vendor.create') }}" class="btn btn-primary">Here</a></h4>
			</div>
		</div>
		@if (count($vendors) !== 0)
			<div class="well" style="background-color: white">
				<table class="table table-hover">
					<thead>
						<th>Name</th>
						<th>Phone</th>
						<th>Address</th>
					</thead>
					<tbody>
						@foreach($vendors as $vendor)
							<tr>
								<td><a href="#">{!! link_to_route('vendor.edit', $vendor->name, [$vendor->id]) !!}</a></td>
								<td>{{$vendor->phone}}</td>
								<td>{{$vendor->address}}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		@else
			<div class="well" style="background-color: white">
			    <div class="page-header">
					<h1>You don't have vendors registered</h1>
				</div>
				<div>
					<p>Create a new vendor.</p>
				</div>
			</div>
		@endif
	</div>
@endsection
