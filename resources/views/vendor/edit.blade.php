@extends('layouts.app')

@section('content')
	<div class="container">
		<a href="{{ URL::previous() }}" class="btn btn-default">Go Back</a><br><br>
		<div class="well" style="background-color: white">
		    <div class="page-header">
				<h1>{{$vendor->name}}</h1>
			</div>
			<div class="container-fluid">
				{!!Form::open(['route'=>['vendor.update', $vendor->id], 'method' => 'put'])!!}

	        {!!Form::label('Name')!!} <i class="required">*</i>
	        {!!Form::text('name', $vendor->name, ['class'=>'form-control', 'placeholder'=>'Name', 'required'=>'true', 'maxlength' => 50])!!}

	        {!!Form::label('Phone')!!} <i class="required">*</i>
	        {!!Form::text('phone', $vendor->phone, ['class'=>'form-control', 'placeholder'=>'Phone', 'required'=>'true', 'maxlength' => 20])!!}

	        {!!Form::label('Address')!!} <i class="required">*</i>
					{!!Form::textarea('address', $vendor->address, ['class'=>'form-control', 'placeholder'=>'Address', 'required'=>'true'])!!}<br>

					{!!Form::label('Zip Code')!!}
					{!!Form::text('zip', $vendor->zip, ['class'=>'form-control', 'placeholder'=>'Zip Code'])!!}<br>

					{!!Form::label('City')!!} <i class="required">*</i>
					{!!Form::select('city_id', $cities, $vendor->city_id, ['class' => 'form-control'])!!}<br>

					{!!Form::button('Save', ['class'=>'btn btn-success','type'=>'submit'])!!}

				{!!Form::close()!!}

				<br>

				{!!Form::open(['route'=> ['vendor.destroy', $vendor->id], 'method' => 'delete'])!!}
						{!!Form::button('Delete ', ['type'=>'submit', 'class' => 'btn btn-danger'])!!}
				{!!Form::close()!!}
			</div>
		</div>
		<a href="{{ URL::previous() }}" class="btn btn-default">Go Back</a><br><br>
	</div>
@endsection
