@extends('layouts.app')

@section('content')

<div class="container">
	<div class="well" style="background-color: white">
			<div class="page-header">
				<h3>Customer: <small>{{ $customer->name }}</small></h3>
			</div>
			<div class="container">
				<img src="/uploads/customers/{{ $customer->picture }}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">
				<form enctype="multipart/form-data" action="/customer/picture/{{$customer->id}}" method="POST">
						<label>Update customer Image</label>
						<input type="file" name="picture">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="submit" class="btn btn-sm btn-primary" value="Save">
				</form>
			</div>
		</div>
		<a href="{{ URL::previous() }}" class="btn btn-default">Go Back</a>
</div>

@endsection
