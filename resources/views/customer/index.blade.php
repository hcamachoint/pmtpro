@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="well" style="background-color: white">
			<div class="container">
				<h4>Add new Customer <a href="{{ route('customer.create') }}" class="btn btn-primary">Here</a></h4>
			</div>
		</div>
		@if (count($customers) !== 0)
			<div class="well" style="background-color: white">
				<table class="table table-hover">
					<thead>
						<th>Photo</th>
						<th>Name</th>
						<th>Phone</th>
						<th>Email</th>
						<th>Action</th>
					</thead>
					<tbody>
						@foreach($customers as $customer)
							<tr>
								<td><img class="img-responsive img-circle" src="/uploads/customers/{{ $customer->picture }}" style="width:45px; height:45px;"></td>
								<td><a href="#">{!! link_to_route('customer.edit', $customer->name, [$customer->id]) !!}</a></td>
								<td>{{$customer->phone}}</td>
								<td>{{$customer->email}}</td>
								<td><a href="#">{!! link_to_route('customer.picture', 'Set Photo', [$customer->id]) !!}</a></td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		@else
			<div class="well" style="background-color: white">
			    <div class="page-header">
					<h1>You don't have customers registered</h1>
				</div>
				<div>
					<p>Create a new customer.</p>
				</div>
			</div>
		@endif
	</div>
@endsection
