@extends('layouts.app')

@section('content')
	<div class="container">
		<a href="{{ URL::previous() }}" class="btn btn-default">Go Back</a><br><br>
		<div class="well" style="background-color: white">
		    <div class="page-header">
				<h1>{{$customer->name}}</h1>
			</div>
			<div class="container-fluid">
				{!!Form::open(['route'=>['customer.update', $customer->id], 'method' => 'put'])!!}

					{!!Form::label('Name')!!} <i class="required">*</i>
					{!!Form::text('name', $customer->name, ['class'=>'form-control', 'placeholder'=>'Name', 'required'=>'true', 'maxlength' => 50])!!}<br>

					{!!Form::label('Address 1')!!} <i class="required">*</i>
					{!!Form::text('address', $customer->address, ['class'=>'form-control', 'maxlength' => 240, 'placeholder'=>'Address 1', 'required'=>'true'])!!}<br>

					{!!Form::label('Address 2')!!}
					{!!Form::text('address2', $customer->address2, ['class'=>'form-control', 'maxlength' => 240, 'placeholder'=>'Address 2'])!!}<br>

					{!!Form::label('Zip Code')!!}
					{!!Form::text('zip', $customer->zip, ['class'=>'form-control', 'placeholder'=>'Zip Code'])!!}<br>

					{!!Form::label('City')!!} <i class="required">*</i>
					{!!Form::select('city', $cities, $customer->city, ['class' => 'form-control', 'maxlength' => 100,])!!}<br>

					{!!Form::label('Email')!!}
					{!!Form::text('email', $customer->email, ['class'=>'form-control', 'placeholder'=>'Email'])!!}<br>

					{!!Form::label('Phone')!!}
					{!!Form::text('phone', $customer->phone, ['class'=>'form-control', 'placeholder'=>'Phone', 'maxlength' => 20])!!}<br>

					{!!Form::button('Save', ['class'=>'btn btn-success','type'=>'submit'])!!}

				{!!Form::close()!!}

				<br>

				{!!Form::open(['route'=> ['customer.destroy', $customer->id], 'method' => 'delete'])!!}
						{!!Form::button('Delete ', ['type'=>'submit', 'class' => 'btn btn-danger'])!!}
				{!!Form::close()!!}
			</div>
		</div>
		<a href="{{ URL::previous() }}" class="btn btn-default">Go Back</a><br><br>
	</div>
@endsection
