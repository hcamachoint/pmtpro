@extends('layouts.app')

@section('content')
	<div class="container">
		<a href="{{ URL::previous() }}" class="btn btn-default">Go Back</a><br><br>
		<div class="well" style="background-color: white">
      <div class="page-header"><h2>New Customer</h2></div>
			{!!Form::open(['route'=>'customer.store', 'method' => 'post', 'files' => true])!!}

        {!!Form::label('Name')!!} <i class="required">*</i>
        {!!Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Name', 'required'=>'true', 'maxlength' => 50])!!}<br>

				{!!Form::label('Address 1')!!} <i class="required">*</i>
				{!!Form::text('address', null, ['class'=>'form-control', 'placeholder'=>'Address 1', 'maxlength' => 240, 'required'=>'true'])!!}<br>

				{!!Form::label('Address 2')!!}
				{!!Form::text('address2', null, ['class'=>'form-control', 'maxlength' => 240, 'placeholder'=>'Address 2'])!!}<br>

				{!!Form::label('Zip Code')!!}
				{!!Form::text('zip', null, ['class'=>'form-control', 'placeholder'=>'Zip Code'])!!}<br>

				{!!Form::label('City')!!} <i class="required">*</i>
				{!!Form::select('city', $cities, null, ['class' => 'form-control', 'maxlength' => 100, 'placeholder' => 'Select a City', 'required' => 'true'])!!}<br>

				{!!Form::label('Email')!!}
				{!!Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Email'])!!}<br>

        {!!Form::label('Phone')!!}
        {!!Form::text('phone', null, ['class'=>'form-control', 'placeholder'=>'Phone', 'maxlength' => 20])!!}<br>

				{!!Form::button('Save', ['class'=>'btn btn-success','type'=>'submit'])!!}

			{!!Form::close()!!}
		</div>
		<a href="{{ URL::previous() }}" class="btn btn-default">Go Back</a><br><br>
	</div>
@endsection
