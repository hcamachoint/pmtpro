<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>PMTPRO</title>
    <link rel="stylesheet" href="{{ asset('/css/chartist.min.css') }}" media="all" />
    <link rel="stylesheet" href="{{ asset('/css/bootstrap.min.css') }}" media="all" />
    <style type="text/css">
      .reader {
        border:none!important;
        outline:none!important;
        background-color:rgba(0, 0, 0, 0);
        -webkit-appearance: none;
        -mox-appearance: none;
      }
      .input-dollar {
        position: relative;
      }
      .input-dollar.left input {
          padding-left:22px;
      }
      .input-dollar.right input {
          padding-right:18px;
          text-align:end;
      }

      .input-dollar:before {
          position: absolute;
          top: 5px;
          content:"$";
          margin-left: 7px;
      }
      .input-dollar.left:before {
          left: 5px;
      }
      .input-dollar.right:before {
          right: 5px;
      }
    </style>
  </head>
  <body>
    <h1 align="center">{{strtoupper($data['projectName'])}}</h1>
    @if($data['projectImage'] != 'default-company.jpg')<img src="uploads/customers/{{$data['projectImage']}}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;margin-top:25px;">@endif
    <div align="right">
      <p><b>Date: </b>{{$data['date']}}</p>
    </div>
    <div align="left">
      <p><b>Username: </b>{{$data['userName']}}</p>
      <p><b>Costumer: </b>{{$data['projectCostumer']}}</p>
    </div>
    <p><b>Project Date:</b> {{$data['projectSdate']}} to {{$data['projectEdate']}}</p>
    <p><b>Project Name:</b> {{$data['projectName']}}</p>
    <p><b>Project Description:</b> {{$data['projectDescription']}}</p>
    <br>

    @if(count($data['materials']) != 0)
      <div class="panel-body">
        <h3>Materials</h3>
        <table id="myTable" class="table order-list">
          <thead>
              <tr class="info">
                <!--<td>Store</td>-->
                <td>Description</td>
                <!--<td>Attachment</td>-->
                <td>Price</td>
                <td>Quantity</td>
                <td>Total</td>
              </tr>
          </thead>
          <tbody>
            @foreach($data['materials'] as $material)
              <tr>
                <td width="60%"><input type="text" class="form-control reader" value="{{$material->description}}"/></td>
                <!--<td><input type="file" name="material_attachment[]{{$loop->iteration}}"></td>-->
                <td width="10%"><span class="input-dollar left"><input type=text class="form-control reader" value="{{number_format($material->price)}}"></span></td>
                <td width="10%"><input type="text" class="form-control reader" value="{{number_format($material->count, 2)}}"/></td>
                <td width="20%"><span class="input-dollar left"><input type="text" class="form-control reader" value="{{number_format($material->price * $material->count, 2)}}"></span></td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    @endif

    @if(count($data['workers']) != 0)
      <div class="panel-body">
        <h3>Workers</h3>
        <table id="myTable" class="table order-list">
          <thead>
              <tr class="info">
                <td>Worker</td>
                <td>Price</td>
                <td>Rate</td>
                <td>Total</td>
              </tr>
          </thead>
          <tbody>
            @foreach($data['workers'] as $worker)
              <tr>
                <td width="60%"><input type="text" class="form-control reader" value="{{$worker->worker->name}}"/></td>
                <!--<td><input type="file" name="worker_attachment[]{{$loop->iteration}}"></td>-->
                <td width="10%"><span class="input-dollar left"><input type=text class="form-control reader" value="{{number_format($worker->price)}}"></span></td>
                <td width="10%"><input type="text" class="form-control reader" value="{{number_format($worker->hour, 2)}}"/></td>
                <td width="20%"><span class="input-dollar left"><input type="text" class="form-control reader" value="{{number_format($worker->price * $Worker->hour, 2)}}"></span></td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    @endif

    <div align="right">
      <h3>Finance</h3>
      <p><b>Original Budget: </b>${{number_format($data['projectBudget'], 2)}}</p>
      <p><b>Net Profit: </b>${{number_format($data['netProfit'], 2)}}</p>
      <p><b>Net Margin: </b>%{{number_format($data['netProfitMargin'], 2)}}</p>
    </div>



    <script type="text/javascript" src="{{ asset('/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/bootstrap.min.js') }}"></script>
  </body>
</html>
