<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>PMTPRO</title>
    <link rel="stylesheet" href="{{ asset('/css/chartist.min.css') }}" media="all" />
  </head>
  <body>
    <h1 align="center">PMTPRO REPORT</h1>
    <h3>{{$data['user']['name']}}</h3>
    <b>Date: </b>{{$data['date']}}
    <br>
    <p><b>Active Projects:</b> {{ $data['actpro'] }}</p>
    <p><b>Closed Projects:</b> {{ $data['clopro'] }}</p>
    <p><b>Finished Projects:</b> {{ $data['finpro'] }}</p>
    <script type="text/javascript" src="{{ asset('/js/pmtpro.min.js') }}"></script>
  </body>
</html>
