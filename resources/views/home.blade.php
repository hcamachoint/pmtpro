@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            <!--MIS PROYECTOS-->
            <div class="panel panel-default">
                <div class="panel-heading">My Projects</div>
                <div class="panel-body">
                  @if (count(Auth::user()->project) !== 0)
                      <table class="table table-hover">
                        <thead>
                          <th>Name</th>
                          <th>Description</th>
                          <th>Budget</th>
                        </thead>
                        <tbody>
                          @foreach(Auth::user()->project as $project)
                            <tr>
                              <td>{!! link_to_route('project.edit', $project->name, [$project->id]) !!}</td>
                              <td>{{$project->description}}</td>
                              <td>${!!Form::text('budget', $project->budget, ['class'=>'reader commifyble', 'readonly' => 'true'])!!}</td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                  @else
                        <p>You don't have projects registered</p>

                        <p>Create a new project <a href="/project/create">here</a>.</p>
                  @endif
                </div>
            </div>

            <!--MIS ESTADISTICAS-->
            @if ($projects != 0)
              <div class="panel panel-default">
                  <div class="panel-heading">Statistics</div>
                  <div class="panel-body">
                    <b>Total Budget: </b>${!!Form::text('totalb', $totalb, ['class'=>'reader commifyble', 'readonly' => 'true'])!!}
                    <b>Total Material: </b>${!!Form::text('totalm', $totalm, ['class'=>'reader commifyble', 'readonly' => 'true'])!!} <br>
                    <b>Total Worker: </b>${!!Form::text('totalw', $totalw, ['class'=>'reader commifyble', 'readonly' => 'true'])!!} 
                    <b>Total Invested: </b>${!!Form::text('totali', $totali, ['class'=>'reader commifyble', 'readonly' => 'true'])!!}
                  </div>
              </div>
            @endif
        </div>
        <!--<div id="example">

        </div>-->
    </div>
</div>
@endsection

@section('footer')
<script src="{{ asset('js/commif.js') }}"></script>
@endsection
