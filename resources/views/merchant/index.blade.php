@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="well" style="background-color: white">
			<div class="container">
				<h4>Add new Merchant <a href="{{ route('merchant.create') }}" class="btn btn-primary">Here</a></h4>
			</div>
		</div>
		@if (count($merchants) !== 0)
			<div class="well" style="background-color: white">
				<table class="table table-hover">
					<thead>
						<th>Name</th>
						<th>Phone</th>
						<th>Address</th>
					</thead>
					<tbody>
						@foreach($merchants as $merchant)
							<tr>
								<td><a href="#">{!! link_to_route('merchant.edit', $merchant->name, [$merchant->id]) !!}</a></td>
								<td>{{$merchant->phone}}</td>
								<td>{{$merchant->address}}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		@else
			<div class="well" style="background-color: white">
			    <div class="page-header">
					<h1>You don't have merchants registered</h1>
				</div>
				<div>
					<p>Create a new merchant.</p>
				</div>
			</div>
		@endif
	</div>
@endsection
