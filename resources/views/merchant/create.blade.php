@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="well" style="background-color: white">
      <div class="page-header"><h2>New Merchant</h2></div>
			{!!Form::open(['route'=>'merchant.store', 'method' => 'post'])!!}

        {!!Form::label('Name')!!} <i class="required">*</i>
        {!!Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Name', 'required'=>'true', 'maxlength' => 50])!!}

        {!!Form::label('Phone')!!} <i class="required">*</i>
        {!!Form::text('phone', null, ['class'=>'form-control', 'placeholder'=>'Phone', 'required'=>'true', 'maxlength' => 20])!!}

        {!!Form::label('Address')!!} <i class="required">*</i>
				{!!Form::textarea('address', null, ['class'=>'form-control', 'placeholder'=>'Address', 'required'=>'true'])!!}<br>

				{!!Form::button('Save', ['class'=>'btn btn-success','type'=>'submit'])!!}

			{!!Form::close()!!}
		</div>
	</div>
@endsection
