@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="well" style="background-color: white">
		    <div class="page-header">
				<h1>{{$merchant->name}}</h1>
			</div>
			<div class="container-fluid">
				{!!Form::open(['route'=>['merchant.update', $merchant->id], 'method' => 'put'])!!}

	        {!!Form::label('Name')!!} <i class="required">*</i>
	        {!!Form::text('name', $merchant->name, ['class'=>'form-control', 'placeholder'=>'Name', 'required'=>'true', 'maxlength' => 50])!!}

	        {!!Form::label('Phone')!!} <i class="required">*</i>
	        {!!Form::text('phone', $merchant->phone, ['class'=>'form-control', 'placeholder'=>'Phone', 'required'=>'true', 'maxlength' => 20])!!}

	        {!!Form::label('Address')!!} <i class="required">*</i>
					{!!Form::textarea('address', $merchant->address, ['class'=>'form-control', 'placeholder'=>'Address', 'required'=>'true'])!!}<br>

					{!!Form::button('Save', ['class'=>'btn btn-success','type'=>'submit'])!!}

				{!!Form::close()!!}

				<br>

				{!!Form::open(['route'=> ['merchant.destroy', $merchant->id], 'method' => 'delete'])!!}
						{!!Form::button('Delete ', ['type'=>'submit', 'class' => 'btn btn-danger'])!!}
				{!!Form::close()!!}
			</div>
		</div>
	</div>
@endsection
