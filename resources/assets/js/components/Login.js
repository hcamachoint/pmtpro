import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

const style = {
  margin: 12,
};

export default class Login extends Component {


    render() {
        return (
          <MuiThemeProvider>
            <Card>
              <CardHeader
                title="Login Page"
                subtitle="Authenticate"
              />
              <CardText>
              <TextField
                hintText="Email Address"
                name="email"
                fullWidth="true"
              /><br />
              <TextField
                hintText="Password"
                type="password"
                name="password"
                fullWidth="true"
              /><br />
              <RaisedButton label="Login" primary={true} style={style} type="submit"/><br />
              </CardText>
            </Card>
          </MuiThemeProvider>
        );
    }
}

if (document.getElementById('login')) {
    ReactDOM.render(<Login />, document.getElementById('login'));
}
