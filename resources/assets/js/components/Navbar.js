import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import FontIcon from 'material-ui/FontIcon';

import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';

import {Tabs, Tab} from 'material-ui/Tabs';

import Avatar from 'material-ui/Avatar';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';

import ContentInbox from 'material-ui/svg-icons/content/inbox';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import ContentSend from 'material-ui/svg-icons/content/send';
import ContentDrafts from 'material-ui/svg-icons/content/drafts';

const styles = {
    marginBottom: "20px"
};

const HeaderMenu = () => (
        <Tabs>
          <Tab label="&nbsp;Home &nbsp;" href="/home"/>
          <Tab label="&nbsp;Profile &nbsp;" href="/profile"/>
        </Tabs>
);

export default class Navbar extends Component {
    /*EL ERROR ESTA EN EL CONSTRUCTOR: THIS.STATE.OPEN(DEPURAR)*/
    constructor(props) {
      super(props);
      this.state = {open: false};
      this.handleToggle= this.handleToggle.bind(this);
    }

    handleToggle(){
      //console.log(this.state);
      this.setState({open: !this.state.open});
    }

    handleClose(){
      this.setState({open: false});
    }

    render() {
        return (
          <MuiThemeProvider>
          <div>
            <AppBar title="PMTPRO" style={styles} iconElementRight={<HeaderMenu/>} onLeftIconButtonClick={this.handleToggle}/>
            <Drawer
              docked={false}
              width={200}
              open={this.state.open}
              onRequestChange={(open) => this.setState({open})}
            >
            <ListItem disabled={true} leftAvatar={<Avatar>A</Avatar>}>
              Auth User
            </ListItem>
            <Divider />
            <List>
              <ListItem
                primaryText="Projects"
                leftIcon={<ContentInbox />}
                initiallyOpen={false}
                primaryTogglesNestedList={true}
                nestedItems={[
                  <ListItem
                    key={1}
                    primaryText="Create"
                    leftIcon={<ContentDrafts />}
                    href="/project/create"
                  />,
                  <ListItem
                    key={2}
                    primaryText="List"
                    leftIcon={<ContentDrafts />}
                    href="/project"
                  />,
                  <ListItem
                    key={3}
                    primaryText="Finished"
                    leftIcon={<ContentDrafts />}
                    href="/project/old"

                  />,
                ]}
              />
              <ListItem primaryText="Customer" leftIcon={<ActionGrade />} href="/customer"/>
              <ListItem primaryText="Labor" leftIcon={<ActionGrade />} href="/labor"/>
              <ListItem primaryText="Vendor" leftIcon={<ActionGrade />} href="/vendor"/>
              <ListItem primaryText="Sign out" leftIcon={<ContentSend />} href="/logout"/>
            </List>
            </Drawer>
          </div>
          </MuiThemeProvider>
        );
    }
}

if (document.getElementById('navbar')) {
    ReactDOM.render(<Navbar />, document.getElementById('navbar'));
}
