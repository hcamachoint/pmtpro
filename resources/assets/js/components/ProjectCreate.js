import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import DatePicker from 'material-ui/DatePicker';
import Divider from 'material-ui/Divider';

import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

const style = {
  margin: 12,
};

const items = [
  <MenuItem key={1} value={1} primaryText="Never" />,
  <MenuItem key={2} value={2} primaryText="Every Night" />,
  <MenuItem key={3} value={3} primaryText="Weeknights" />,
  <MenuItem key={4} value={4} primaryText="Weekends" />,
  <MenuItem key={5} value={5} primaryText="Weekly" />,
];

export default class ProjectCreate extends Component {
    constructor(props) {
      super(props);
      this.state = {value: null};
      this.handleChange= this.handleChange.bind(this);
    }

    handleChange(event, index, value){this.setState({value});}

    render() {
        return (
          <MuiThemeProvider>
            <Card>
              <CardHeader
                title="Project Create"
              />
              <CardText>
                <TextField
                  hintText="Customer Name"
                  name="cname"
                  fullWidth="true"
                  floatingLabelText="Customer Name"
                /><br />
                <TextField
                  hintText="Customer Address"
                  name="caddress"
                  fullWidth="true"
                  floatingLabelText="Customer Addess"
                /><br />
                <SelectField
                  value={this.state.value}
                  onChange={this.handleChange}
                  floatingLabelText="Customer City"
                  name="ccity"
                >
                  {items}
                </SelectField><br />

                <DatePicker floatingLabelText="Start Date" hintText="Start Date" mode="landscape" container="inline" name="sdate"/><br />
                <DatePicker floatingLabelText="End Date" hintText="End Date" mode="landscape" container="inline" name="edate"/><br />
                <TextField
                  hintText="Budget"
                  name="budget"
                  fullWidth="true"
                  floatingLabelText="Project Budget"
                /><br /><br />
                <TextField
                  hintText="Name"
                  name="name"
                  fullWidth="true"
                  floatingLabelText="Project Name"
                /><br />
                <TextField
                  hintText="Description"
                  name="description"
                  fullWidth="true"
                  multiLine="true"
                  rows="3"
                  floatingLabelText="Project Description"
                /><br />
                <RaisedButton label="Save" primary={true} style={style} type="submit"/><br />
              </CardText>
            </Card>
          </MuiThemeProvider>
        );
    }
}

if (document.getElementById('projectc')) {
    ReactDOM.render(<ProjectCreate />, document.getElementById('projectc'));
}
