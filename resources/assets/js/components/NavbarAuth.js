import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import FlatButton from 'material-ui/FlatButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import MenuItem from 'material-ui/MenuItem';
import FontIcon from 'material-ui/FontIcon';

const HeaderMenu = () => (
  <IconMenu
    iconButtonElement={
      <IconButton><MoreVertIcon /></IconButton>
    }
    targetOrigin={{horizontal: 'right', vertical: 'top'}}
    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
  >
    <MenuItem primaryText="Register" href="/register" />
    <MenuItem primaryText="Login" href="/login" />
  </IconMenu>
);

const styles = {
    paddingLeft: "20px",
    marginBottom: "20px"
};

export default class NavbarAuth extends Component {
    render() {
        return (
          <MuiThemeProvider>
            <AppBar title="PMTPRO" style={styles} iconElementRight={<HeaderMenu/>} iconElementLeft={<IconButton></IconButton>}/>
          </MuiThemeProvider>
        );
    }
}

if (document.getElementById('navbarAuth')) {
    ReactDOM.render(<NavbarAuth />, document.getElementById('navbarAuth'));
}
