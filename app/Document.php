<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $with = ['category'];
    protected $table = 'documents';
    public $timestamps = true;
    protected $fillable = array('description', 'type', 'file_path', 'file_name', 'edate', 'category_id', 'documentable_id', 'documentable_type');
    protected $hidden = ['documentable_id', 'documentable_type', 'category_id', 'created_at', 'updated_at'];

    public function documentable()
    {
        return $this->morphTo();
    }

    public function category()
    {
        return $this->hasOne('App\DocumentCategory', 'id', 'category_id');
    }

}
