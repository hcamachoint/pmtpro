<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merchant extends Model 
{

    protected $table = 'merchants';
    public $timestamps = true;
    protected $fillable = array('name', 'address', 'phone');

    public function projects()
    {
        return $this->belongsToMany('App\Project');
    }

}