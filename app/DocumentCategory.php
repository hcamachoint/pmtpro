<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentCategory extends Model
{

    protected $table = 'document_categories';
    public $timestamps = true;
    protected $fillable = array('name', 'description');
    protected $hidden = ['created_at', 'updated_at'];

    public function document()
    {
        return $this->hasMany('App\Document', 'id');
    }

}
