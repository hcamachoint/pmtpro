<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
  protected $hidden = ['state_id', 'created_at', 'updated_at'];
  protected $with = ['state'];
  protected $table = 'cities';
  public $timestamps = true;
  protected $fillable = array('title', 'slug', 'state_id');

  public function state()
  {
    return $this->hasOne('App\State', 'id', 'state_id');
  }
}
