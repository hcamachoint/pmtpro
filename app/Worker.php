<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Worker extends Model
{

    protected $table = 'workers';
    public $timestamps = true;
    protected $fillable = array('idn', 'name', 'phone', 'email', 'jobtitle', 'address', 'city', 'zip');

    public function projects()
    {
        return $this->belongsToMany('App\Project');
    }

    public function document()
    {
        return $this->morphMany('App\Document', 'documentable');
    }

}
