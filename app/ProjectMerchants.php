<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectMerchants extends Model 
{

    protected $table = 'project_merchants';
    public $timestamps = true;
    protected $fillable = array('product', 'price', 'count', 'attachment');

}