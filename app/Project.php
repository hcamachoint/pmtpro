<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{

    protected $table = 'projects';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $fillable = array('name', 'description', 'sdate', 'edate', 'budget', 'fbudget', 'status', 'customer_id');

    public function customers()
    {
        return $this->hasOne('App\Customer', 'id', 'customer_id');
    }

    public function workers()
    {
        return $this->hasMany('App\ProjectWorker');
    }

    public function materials()
    {
        return $this->hasMany('App\ProjectMaterial');
    }

    public function merchants()
    {
        return $this->hasMany('App\Merchant');
    }

    public function vendors()
    {
        return $this->hasMany('App\Vendor');
    }

    public function budgets()
    {
        return $this->hasMany('App\ProjectBudgets');
    }

}
