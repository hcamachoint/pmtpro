<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{

    protected $table = 'vendors';
    public $timestamps = true;
    protected $fillable = array('name', 'phone', 'address', 'city_id', 'zip');

    public function projects()
    {
        return $this->belongsToMany('App\Project');
    }

}
