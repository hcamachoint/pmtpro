<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function project()
    {
        return $this->hasMany('App\Project');
    }

    public function customer()
    {
        return $this->hasMany('App\Customer');
    }

    public function worker()
    {
        return $this->hasMany('App\Worker');
    }

    public function vendor()
    {
        return $this->hasMany('App\Vendor');
    }

    public function merchant()
    {
        return $this->hasMany('App\Merchant');
    }
}
