<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectMaterial extends Model 
{

    protected $table = 'project_materials';
    public $timestamps = true;
    protected $fillable = array('description', 'price', 'count', 'attachment', 'project_id');
    protected $visible = array('description', 'price', 'count', 'attachment', 'project_id');

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

}
