<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectBudgets extends Model 
{

    protected $table = 'project_budgets';
    public $timestamps = true;
    protected $fillable = array('description', 'amount');

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

}