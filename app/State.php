<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
  protected $table = 'states';
  public $timestamps = true;
  protected $fillable = array('title', 'slug', 'country_id');
  protected $hidden = ['id', 'country_id', 'created_at', 'updated_at'];
  protected $with = ['country'];

  public function country()
  {
      return $this->hasOne('App\Country', 'id', 'country_id');
  }
}
