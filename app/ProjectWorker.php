<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectWorker extends Model
{

    protected $table = 'project_workers';
    public $timestamps = true;
    protected $fillable = array('hour', 'price', 'worker_id', 'project_id');

    public function labor()
    {
        return $this->hasOne('App\ProjectWorker', 'id', 'worker_id');
    }

}
