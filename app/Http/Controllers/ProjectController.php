<?php

namespace App\Http\Controllers;

use Session;
use Redirect;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;
use App\Customer;
use App\Worker;
use App\City;
use App\Project;
use App\ProjectWorker;
use App\ProjectMaterial;
use App\Http\Requests\ProjectCreateRequest;
use App\Http\Requests\ProjectUpdateRequest;
use Illuminate\Support\Facades\Input;

class ProjectController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index()
  {
    $user = auth()->user();
    return view('project.index', ['projects' => $user->project->where('status', 1)]);
  }

  public function create()
  {
    $user = auth()->user();
    //Verifico si existen workers
    if (count($user->worker) == 0) {
      $lb = 'hidden';
    }else {
      $lb = 'visible';
    }
    $cm = count($user->customer);
    $cities = City::all('title', 'id');
    return view('project.create', ['customers' => $user->customer->sortBy('name', SORT_NATURAL|SORT_FLAG_CASE)->pluck('name', 'id'), 'workers' => $user->worker->sortBy('name', SORT_NATURAL|SORT_FLAG_CASE)->pluck('name', 'id'), 'cities' => $cities->sortBy('title', SORT_NATURAL|SORT_FLAG_CASE)->pluck('title', 'id'), 'lb' => $lb, 'cm' => $cm]);
  }

  public function store(ProjectCreateRequest $request)
  {
    if (!request()->has('customer_id')) {
      try {
        $customer = new Customer;
        $customer->name = $request->cname;
        $customer->address = $request->caddress;
        $customer->city = $request->ccity;
        $customer->user_id = auth()->user()->id;
        $customer->save();
      } catch (\Exception $e) {
        return Redirect::back()->with('message-error', 'An error occurred while trying to process your request.'. $e);
      }
      $cust = $customer->id;
    }else {
      $cust = $request->customer_id;
    }
    try {
      //PROYECTO
      $project = new Project;
      $project->fill($request->all());
      $project->customer_id = $cust;
      $project->user_id = auth()->user()->id;
      $project->save();
    } catch (\Exception $e) {
      return Redirect::back()->with('message-error', 'An error occurred while trying to process your request.'. $e);
    }

    //MATERIALES
    if (request()->has('material_description')) {
      foreach($request->material_description as $index => $value) {
        $material = new ProjectMaterial;

        if (request()->has('material_attachment')) {
          $file = $request->material_attachment[$index];
          $fileName = $file->getClientOriginalName();
          $filePath = url('uploads/documents/'.$fileName);
          $destinationPath = 'uploads/documents';
          if($file->move($destinationPath,$fileName)) {
            $material->attachment = $filePath;
          }else {
            return Redirect()->route('project.index')->with('message', 'Project created correctly, but can save the project materials!');
          }
        }

        $material->project_id = $project->id;
        $material->description = $request->material_description[$index];
        $material->price = floatval(str_replace(',', '', $request->material_price[$index]));
        $material->count = floatval(str_replace(',', '', $request->material_count[$index]));
        $material->save();

      }
    }

    //LABORS
    if (request()->has('worker_id')) {
      foreach($request->worker_id as $index => $value) {
          $worker = new ProjectWorker;
          $worker->project_id = $project->id;
          $worker->worker_id = $request->worker_id[$index];
          $worker->price = floatval(str_replace(',', '', $request->worker_price[$index]));
          $worker->hour = floatval(str_replace(',', '', $request->worker_hours[$index]));
          $worker->save();
      }
    }
    //Verifico si existen workers

    return Redirect()->route('project.index')->with('message', 'Project created correctly!');
  }

  public function show($id)
  {
    try {
      $project = Project::find($id);
    } catch (\Exception $e) {
      return Redirect::back()->with('message-error', 'An error occurred while trying to process your request.');
    }
    if (empty($project)) {
      return Redirect::back()->with('message-error', 'Project not found!');
    }else{
      $user = auth()->user();
      $lb = count($user->worker);
      $pl = count($project->workers);
      $pm = count($project->materials);
      if (count($user->worker) == 0) {
        $lb = 'hidden';
      }else {
        $lb = 'visible';
      }
      return view('project.show', ['project' => $project, 'projectMaterials' => $project->materials, 'projectWorkers' => $project->workers, 'customers' => $user->customer->pluck('name', 'id'), 'workers' => $user->worker->pluck('name', 'id'), 'lb' => $lb, 'pl' => $pl, 'pm' => $pm, 'lb' => $lb]);
    }
  }

  public function edit($id)
  {
    try {
      $project = Project::find($id);
    } catch (\Exception $e) {
      return Redirect::back()->with('message-error', 'An error occurred while trying to process your request.');
    }
    if (empty($project)) {
      return Redirect::back()->with('message-error', 'Project not found!');
    }else{
      $user = auth()->user();
      $lb = count($user->worker);
      $pl = count($project->workers);
      $pm = count($project->materials);
      if (count($user->worker) == 0) {
        $lb = 'hidden';
      }else {
        $lb = 'visible';
      }
      $cities = City::all('title', 'id');
      return view('project.edit', ['project' => $project, 'projectMaterials' => $project->materials, 'projectWorkers' => $project->workers, 'customers' => $user->customer->sortBy('name', SORT_NATURAL|SORT_FLAG_CASE)->pluck('name', 'id'), 'workers' => $user->worker->sortBy('name', SORT_NATURAL|SORT_FLAG_CASE)->pluck('name', 'id'), 'cities' => $cities->sortBy('title', SORT_NATURAL|SORT_FLAG_CASE)->pluck('title', 'id'), 'lb' => $lb, 'pl' => $pl, 'pm' => $pm, 'lb' => $lb]);
    }

  }

  public function update(ProjectUpdateRequest $request, $id)
  {
    try {
      $project = Project::find($id);
    } catch (\Exception $e) {
      return Redirect::back()->with('message-error', 'An error occurred while trying to process your request.');
    }
    if (empty($project)) {
      return Redirect::back()->with('message-error', 'Project not found!');
    }elseif ($project->user_id != auth()->user()->id) {
      return Redirect::back()->with('message-error', 'Unauthorized action!');
    }elseif ($project->status == 2 || $project->status == 3) {
      return Redirect::back()->with('message-error', 'You can not edit a closed / finalized project!');
    }else{
      $project->fill($request->all());
      $project->save();
      return Redirect()->route('project.index')->with('message', 'Project updated correctly!');
    }
  }

  public function destroy($id)
  {
    try {
      $project = Project::find($id);
    } catch (\Exception $e) {
      return Redirect::back()->with('message-error', 'An error occurred while trying to process your request.');
    }
    if (empty($project)) {
      return Redirect::back()->with('message-error', 'Project not found!');
    }else{
      $status = $project->status;
      $project->delete();
      if ($status == 2 || $status == 3) {
        return Redirect()->route('project.old')->with('message', 'Project removed correctly!');
      }
      return Redirect()->route('project.index')->with('message', 'Project removed correctly!');
    }
  }

  public function finish($id)
  {
    try {
      $project = Project::find($id);
    } catch (\Exception $e) {
      return Redirect::back()->with('message-error', 'An error occurred while trying to process your request.');
    }
    if (empty($project)) {
      return Redirect::back()->with('message-error', 'Project not found!');
    }else{
      $project->status = 3;
      $project->save();
      return Redirect()->route('project.index')->with('message', 'Project finished correctly. You can see the completed projects in [Completed Projects]!');
    }
  }

  public function close($id)
  {
    try {
      $project = Project::find($id);
    } catch (\Exception $e) {
      return Redirect::back()->with('message-error', 'An error occurred while trying to process your request.');
    }
    if (empty($project)) {
      return Redirect::back()->with('message-error', 'Project not found!');
    }else{
      $project->status = 2;
      $project->save();
      return Redirect()->route('project.index')->with('message', 'Project closed correctly. You can see the closed projects in [Completed Projects]!');
    }
  }

  public function open($id)
  {
    try {
      $project = Project::find($id);
    } catch (\Exception $e) {
      return Redirect::back()->with('message-error', 'An error occurred while trying to process your request.');
    }
    if (empty($project)) {
      return Redirect::back()->with('message-error', 'Project not found!');
    }else{
      $project->status = 1;
      $project->save();
      return Redirect()->route('project.index')->with('message', 'Project opened correctly!');
    }
  }

  public function old()
  {
    $user = auth()->user();
    return view('project.old', ['projects' => $user->project->whereIn('status', [2,3])]);
  }

  public function worker(Request $request)
  {
    $this->validate($request, [
      'name' => 'required | string | max:50',
      'address' => 'required | string',
      'city' => 'required | string',
    ]);
    try {
      $worker = new Worker;
      $worker->fill($request->all());
      $worker->user_id = auth()->user()->id;
      $worker->city = intval($request->city);
      $worker->save();
    } catch (\Exception $e) {
      return response()->json([
          'error' => $e
      ]);
    }

    return response()->json([
        'worker' => $worker
    ]);
  }

  public function customer(Request $request)
  {
    $this->validate($request, [
      'name' => 'required | string | max:50',
      'address' => 'required | string',
      'city' => 'required | string',
    ]);

    try {
      $customer = new Customer;
      $customer->name = $request->name;
      $customer->address = $request->address;
      $customer->city = intval($request->city);
      $customer->user_id = auth()->user()->id;
      $customer->save();
    } catch (\Exception $e) {
      return response()->json([
          'error' => $e
      ]);
    }

    return response()->json([
        'customer' => $customer
    ]);
  }

  public function pdf(Request $request, $id)
  {
    try {
      $project = Project::find($id);
    } catch (\Exception $e) {
      return Redirect::back()->with('message-error', 'An error occurred while trying to process your request.');
    }
    if (empty($project)) {
      return Redirect::back()->with('message-error', 'Project not found!');
    }else{
      $total = 0;
      foreach ($project->materials as $key => $value) {
        $total += ($value->price * $value->count);
      }
      foreach ($project->workers as $key => $value) {
        $total += ($value->price * $value->hour);
      }

      $data =  [
        'userName' => auth()->user()->name,
        'date' => Carbon::now()->toDateTimeString(),
        'projectCostumer' => $project->customers->name,
        'projectImage' => $project->customers->picture,
        'projectName' => $project->name,
        'projectDescription' => $project->description,
        'projectBudget' => $project->budget,
        'projectSdate' => $project->sdate,
        'projectEdate' => $project->edate,
        'workers' => $project->workers,
        'materials' => $project->materials,
        'netProfit' => $project->budget - $total,
        'netProfitMargin' => (($project->budget - $total) * 100) / $project->budget,
      ];

      $pdf = \PDF::loadView('pdf.project', compact('data'));
      return $pdf->stream();
      //return $pdf->download('pmtpro_'.Carbon::now()->toDateString().'.pdf');
    }
  }
}

?>
