<?php

namespace App\Http\Controllers;

use Session;
use Redirect;
use Image;
use Illuminate\Http\Request;
use App\Worker;
use App\City;

class WorkerController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $user = auth()->user();
    return view('worker.index', ['workers' => $user->worker]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    $cities = City::all('title', 'id');
    return view('worker.create', ['cities' => $cities->sortBy('title')->pluck('title', 'id')]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'idn' => request()->has('idn') ? '' : 'regex:"/^[A-Z]{1,2}(?)[0-9]{12}$/" | unique:workers,idn,'.$request->user()->id,
      'name' => 'required | string | max:50',
      'jobtitle' => request()->has('jobtitle') ? '' : 'required | string | max:50',
      'address' => 'required | string',
      'city' => 'required | integer | between:1,20',
      'zip' => request()->has('zip') ? '' : 'required | string | between:1,10',
      'phone' => request()->has('phone') ? '' : 'required | digits_between:10,20',
      'email' => request()->has('email') ? '' : 'required | email | string | between:3,200',
    ]);
    try {
      $worker = new Worker;
      $worker->fill($request->all());
      $worker->user_id = auth()->user()->id;
      $worker->save();
    } catch (\Exception $e) {
      return Redirect::back()->with('message-error', 'An error occurred while trying to process your request.'. $e);
    }
    return Redirect()->route('worker.index')->with('message', 'Worker registered correctly!');
  }

  public function edit($id)
  {
    $cities = City::all('title', 'id');
    $worker = Worker::find($id);
    return view('worker.edit', ['worker' => $worker, 'cities' => $cities->sortBy('title')->pluck('title', 'id')]);
  }

  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'idn' => request()->has('idn') ? '' : 'required | regex:"/^[A-Z]{1,2}(?)[0-9]{12}$/" | unique:workers,idn,'.$request->user()->id,
      'name' => 'required | string | max:50',
      'jobtitle' => request()->has('jobtitle') ? '' : 'required | string | max:50',
      'address' => 'required | string',
      'city' => request()->has('city') ? '' : 'required | integer | between:1,20',
      'zip' => request()->has('zip') ? '' : 'required | string | between:1,10',
      'phone' => request()->has('phone') ? '' : 'required | digits_between:10,20',
      'email' => request()->has('email') ? '' : 'required | email | string | between:3,200',
    ]);
    try {
      $worker = Worker::find($id);
      if (auth()->user()->id === $worker->user_id) {
        $worker->fill($request->all());
        $worker->save();
        return Redirect()->route('worker.index')->with('message', 'Worker updated correctly!');
      }else{
          return abort(403, 'Unauthorized action.');
      }
    } catch (\Exception $e) {
      Session::flash('message-error', 'An error occurred while trying to process your request.');
      return Redirect::back();
    }
  }

  public function destroy($id)
  {
    try {
      $worker = Worker::find($id);
      if (auth()->user()->id === $worker->user_id) {
          if ($worker->picture != '' && $worker->picture != 'default.jpg') {
            unlink(public_path('uploads/workers/'.$worker->picture));
          }

          $worker->delete();
          Session::flash('message', 'Worker deleted properly!');
          return Redirect()->route('worker.index');
      }else{
          return abort(403, 'Unauthorized action.');
      }
    } catch (\Exception $e) {
      Session::flash('message-error', 'An error occurred while trying to process your request.');
      return Redirect::back();
    }

  }

  public function picture($id)
  {
    $worker = Worker::find($id);
    return view('worker.picture', ['worker' => $worker]);
  }

  public function setPicture(Request $request, $id)
  {
      $this->validate($request, [
          'picture' => 'required | dimensions:min_width=100,min_height=100 | image',
      ]);

      try {
          $worker = Worker::find($id);
          if (auth()->user()->id === $worker->user_id) {
            $picture = $request->file('picture');
            $filename = hash('ripemd160', $request->user()). '_' . time() . '.' . $picture->getClientOriginalExtension();
            Image::make($picture)->resize(300, 300)->save( public_path('uploads/workers/' . $filename ) );

            try {
                $worker->picture = $filename;
                $worker->save();
                return Redirect()->route('worker.index')->with('message', 'Photo successfully updated!');
            } catch (Exception $e) {
                Session::flash('message-error', 'An error occurred while trying to process your request');
                return Redirect::back();
            }

          }else{
              return abort(403, 'Unauthorized action.');
          }
      } catch (Exception $e) {
          Session::flash('message-error', 'An error occurred while trying to process your request');
          return Redirect::back();
      }
  }

}

?>
