<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
use App\Models\Document;
use App\Models\User;
use App\Models\Work;

class DocumentController extends Controller
{
  /**
  * @SWG\Get(
  *     path="/document/{cod}",
  *     summary="Vista de metodo documento especifico",
  *     operationId="listPets",
  *     tags={"DocumentController - View"},
  *     @SWG\Parameter(
  *         name="authorization",
  *         in="header",
  *         description="Bearer [TOKEN_KEY]",
  *         required=true,
  *         type="string"
  *     ),
  *     @SWG\Parameter(
  *         name="cod",
  *         in="path",
  *         description="ID del documento a consultar",
  *         required=true,
  *         type="string"
  *     ),
  *     @SWG\Response(
  *         response=200,
  *         description="Arreglo del documento especifico.",
  *     ),
  *     @SWG\Response(
  *         response=500,
  *         description="Error interno del servidor",
  *     ),
  *     @SWG\Response(
  *         response="default",
  *         description="Error inesperado",
  *     )
  * )
  */
  public function view($cod){
    try {
        $document = Document::find($cod);
        return response()->json(['success' => true, 'document' => $document], 200);
    } catch (JWTException $e) {
        return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
    }
  }

  /**
  * @SWG\Get(
  *     path="/document",
  *     summary="Lista de todas los docuementos registradas en el sistema",
  *     operationId="listPets",
  *     tags={"DocumentController - List"},
  *     @SWG\Parameter(
  *         name="authorization",
  *         in="header",
  *         description="Bearer [TOKEN_KEY]",
  *         required=true,
  *         type="string"
  *     ),
  *     @SWG\Response(
  *         response=200,
  *         description="Arreglo de todos los documentos del usuario logueado",
  *     ),
  *     @SWG\Response(
  *         response=500,
  *         description="Error interno del servidor",
  *     ),
  *     @SWG\Response(
  *         response="default",
  *         description="Error inesperado",
  *     )
  * )
  */
  public function list(){
    try {
        $documents = $request->user()->document;
        return response()->json(['success' => true, 'documents' => $documents], 200);
    } catch (JWTException $e) {
        return response()->json(['success' => false, 'error' => 'Failed to logout, please try again.'], 500);
    }
  }

  /**
  * @SWG\Post(
  *     path="/document/{id}",
  *     summary="Registro de documentos",
  *     operationId="listPets",
  *     tags={"DocumentController - Store"},
  *     @SWG\Parameter(
  *         name="authorization",
  *         in="header",
  *         description="Bearer [TOKEN_KEY]",
  *         required=true,
  *         type="string"
  *     ),
  *     @SWG\Parameter(
  *         name="id",
  *         in="path",
  *         description="ID del usuario/trabajo a vincular",
  *         required=true,
  *         type="string"
  *     ),
  *     @SWG\Parameter(
  *         name="category",
  *         in="query",
  *         description="ID de la categoria del metodo de contacto",
  *         required=true,
  *         type="string"
  *     ),
  *     @SWG\Parameter(
  *         name="description",
  *         in="query",
  *         description="Descripcion del documento",
  *         required=true,
  *         type="string"
  *     ),
  *     @SWG\Parameter(
  *         name="able",
  *         in="query",
  *         description="Indica si es usuario(1) o un post(2) a donde se va vincular el metodo de contacto.",
  *         required=true,
  *         type="string"
  *     ),
  *     @SWG\Parameter(
  *         name="document",
  *         in="formData",
  *         description="Archivo del documento: jpg,jpeg,png,doc,docx,pdf,odt(Maximo 4mb)",
  *         required=true,
  *         type="file"
  *     ),
  *     @SWG\Response(
  *         response=201,
  *         description="Se registro el documento correctamente.",
  *     ),
  *     @SWG\Response(
  *         response=400,
  *         description="El documento ya existe",
  *     ),
  *     @SWG\Response(
  *         response=403,
  *         description="Accion no autorizada",
  *     ),
  *     @SWG\Response(
  *         response=405,
  *         description="Error en la validacion del formulario",
  *     ),
  *     @SWG\Response(
  *         response=500,
  *         description="Error interno del servidor",
  *     ),
  *     @SWG\Response(
  *         response="default",
  *         description="Error inesperado",
  *     )
  * )
  */
  public function store(Request $request, $id)
  {
      $rules = [
          'description' => 'string | between:1,50',
          'document' => 'required | mimes:jpg,jpeg,png,doc,docx,pdf,odt | max:4096',
          'category' => 'required | integer',
          'able' => 'required | integer | between:1,2'
      ];
      $input = $request->only(
          'document',
          'able',
          'category',
          'description'
      );
      $validator = Validator::make($input, $rules);
      if($validator->fails()) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 405);
      }

      if ($request->able == 1) {
          $able = 'App\Models\User';
          $ac = User::find($id);
          if (auth()->user()->id != $id) {
              return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
          }
      }else{
          $able = 'App\Models\Work';
          $ac = Work::find($id);
          if (auth()->user()->id != $ac->user->id) {
              return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
          }
      }


      if (!empty($request->document)) {
          //VERIFICAR EL ARCHIVO
          $file = $request->document;
          $fileName = hash('ripemd160', $request->user()). '_' . time() . '.' . $file->getClientOriginalExtension();
          $filePath = url('uploads/documents/'.$fileName);
          $destinationPath = 'uploads/documents';

          if ($file->extension() == 'jpg' || $file->extension() == 'jpeg' || $file->extension() == 'png') {
            $fileType = 1;
          } elseif ($file->extension() == 'pdf') {
            $fileType = 2;
          } elseif ($file->extension() == 'doc' || $file->extension() == 'docx') {
            $fileType = 3;
          }

          //INTENTAR GUARDAR LOS CAMBIOS
          try {
              if($file->move($destinationPath,$fileName)) {
                $doc = new Document;
                $doc->description = $request->description;
                $doc->category_id = $request->category;
                $doc->file_path = $filePath;
                $doc->file_name = $fileName;
                $doc->type = $fileType;
                $doc->documentable_id = $id;
                $doc->documentable_type = $able;
                $doc->save();
                return response()->json(['success'=> true, 'message'=> 'Document assigned correctly!', 'item' => $doc], 201);
              }
          } catch (Exception $e) {
              $error_message = $e->getMessage();
              return response()->json(['success' => false, 'error' => $error_message], 500);
          }
      }else{
          return response()->json(['success' => false, 'error' => 'That assigned Document already exists.'], 400);
      }
  }

  /**
  * @SWG\Put(
  *     path="/document/{id}/{cod}",
  *     summary="Actualizacion de documentos",
  *     operationId="listPets",
  *     tags={"DocumentController - Update"},
  *     @SWG\Parameter(
  *         name="authorization",
  *         in="header",
  *         description="Bearer [TOKEN_KEY]",
  *         required=true,
  *         type="string"
  *     ),
  *     @SWG\Parameter(
  *         name="id",
  *         in="path",
  *         description="ID del usuario/trabajo a vincular",
  *         required=true,
  *         type="string"
  *     ),
  *     @SWG\Parameter(
  *         name="cod",
  *         in="path",
  *         description="Codigo del documento a modificar",
  *         required=true,
  *         type="string"
  *     ),
  *     @SWG\Parameter(
  *         name="category",
  *         in="query",
  *         description="ID de la categoria del metodo de contacto",
  *         required=false,
  *         type="string"
  *     ),
  *     @SWG\Parameter(
  *         name="description",
  *         in="query",
  *         description="Descripcion del documento",
  *         required=false,
  *         type="string"
  *     ),
  *     @SWG\Parameter(
  *         name="able",
  *         in="query",
  *         description="Indica si es usuario(1) o un post(2) a donde se va vincular el metodo de contacto.",
  *         required=true,
  *         type="string"
  *     ),
  *     @SWG\Parameter(
  *         name="document",
  *         in="formData",
  *         description="Archivo del documento: jpg,jpeg,png,doc,docx,pdf,odt(Maximo 4mb)",
  *         required=false,
  *         type="file"
  *     ),
  *     @SWG\Response(
  *         response=201,
  *         description="Se registro el documento correctamente.",
  *     ),
  *     @SWG\Response(
  *         response=400,
  *         description="El documento no existe",
  *     ),
  *     @SWG\Response(
  *         response=403,
  *         description="Accion no autorizada",
  *     ),
  *     @SWG\Response(
  *         response=405,
  *         description="Error en la validacion del formulario",
  *     ),
  *     @SWG\Response(
  *         response=500,
  *         description="Error interno del servidor",
  *     ),
  *     @SWG\Response(
  *         response="default",
  *         description="Error inesperado",
  *     )
  * )
  */
  public function update(Request $request, $id, $cod)
  {
      $rules = [];
      if (!empty($request->document)) {
        $rules['document'] = 'mimes:jpg,jpeg,png,doc,docx,pdf,odt | max:4096';
      }
      if (!empty($request->category)) {
        $rules['category'] = 'integer';
      }
      if (!empty($request->description)) {
        $rules['description'] = 'required | string | between:1,50';
      }

      $rules['able'] = 'required | integer | between:1,2';
      $input = $request->only(
          'document',
          'category',
          'description',
          'able'
      );
      $validator = Validator::make($input, $rules);
      if($validator->fails()) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 405);
      }

      try {
          $doc = Document::find($cod);
      } catch (JWTException $e) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 500);
      }

      if (empty($doc)) {
        return response()->json(['success' => false, 'error' => 'Document not found.'], 400);
      }else{
        $oldDoc = $doc->file_name;
        if ($request->able == 1) {
            if ($request->user()->id != $doc->documentable_id || $request->user()->id != $id) {
                return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
            }
        }else{
            $work = Work::find($id);
            if ($request->user()->id != $work->user_id) {
                return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
            }
        }

        if (!empty($request->document)) {
            //VERIFICAR EL ARCHIVO
            $file = $request->document;
            $fileName = hash('ripemd160', $request->user()). '_' . time() . '.' . $file->getClientOriginalExtension();
            $filePath = url('uploads/documents/'.$fileName);
            $destinationPath = 'uploads/documents';


            $doc->file_path = $filePath;
            $doc->file_name = $fileName;
            if ($file->extension() == 'jpg' || $file->extension() == 'jpeg' || $file->extension() == 'png') {
              $doc->type = 1;
            } elseif ($file->extension() == 'pdf') {
              $doc->type = 2;
            } elseif ($file->extension() == 'doc' || $file->extension() == 'docx') {
              $doc->type = 3;
            }
            $file->move($destinationPath,$fileName);
        }

        if (!empty($request->description)) {
          $doc->description = $request->description;
        }

        if (!empty($request->category)) {
          $doc->category_id = $request->category;
        }

        //INTENTAR GUARDAR LOS CAMBIOS
        try {
            $doc->save();
        } catch (Exception $e) {
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 500);
        }
        File::delete('uploads/documents/'.$oldDoc);
        return response()->json(['success'=> true, 'message'=> 'Document updated correctly!'], 200);
      }

  }

  /**
  * @SWG\Delete(
  *     path="/document/{id}/{cod}",
  *     summary="Eliminar documento",
  *     operationId="listPets",
  *     tags={"DocumentController - Destroy"},
  *     @SWG\Parameter(
  *         name="authorization",
  *         in="header",
  *         description="Bearer [TOKEN_KEY]",
  *         required=true,
  *         type="string"
  *     ),
  *     @SWG\Parameter(
  *         name="id",
  *         in="path",
  *         description="ID del usuario/trabajo a vincular",
  *         required=true,
  *         type="string"
  *     ),
  *     @SWG\Parameter(
  *         name="cod",
  *         in="path",
  *         description="Codigo del documento a eliminar",
  *         required=true,
  *         type="string"
  *     ),
  *     @SWG\Parameter(
  *         name="able",
  *         in="query",
  *         description="Indica si es usuario(1) o un post(2) a donde se va vincular el metodo de contacto.",
  *         required=true,
  *         type="string"
  *     ),
  *     @SWG\Response(
  *         response=200,
  *         description="Documento eliminado exitosamente",
  *     ),
  *     @SWG\Response(
  *         response=400,
  *         description="Error, documento no encontrado",
  *     ),
  *     @SWG\Response(
  *         response=403,
  *         description="Accion no autorizada",
  *     ),
  *     @SWG\Response(
  *         response=405,
  *         description="Error en la validacion del formulario",
  *     ),
  *     @SWG\Response(
  *         response=500,
  *         description="Error interno del servidor",
  *     ),
  *     @SWG\Response(
  *         response="default",
  *         description="Error inesperado",
  *     )
  * )
  */
  public function destroy(Request $request, $id, $cod)
  {
      $rules = [
          'able' => 'required | integer | between:1,2'
      ];
      $input = $request->only(
          'able'
      );
      $validator = Validator::make($input, $rules);
      if($validator->fails()) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 405);
      }

      try {
          $doc = Document::find($cod);
      } catch (JWTException $e) {
          $error = $validator->messages()->toJson();
          return response()->json(['success'=> false, 'error'=> $error], 500);
      }

      if (empty($doc)) {
        return response()->json(['success' => false, 'error' => 'Document not found.'], 400);
      }else{
        if ($request->able == 1) {
            if ($request->user()->id != $doc->documentable_id || $request->user()->id != $id) {
                return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
            }
        }else{
            $work = Work::find($id);
            if ($request->user()->id != $work->user_id) {
                return response()->json(['success' => false, 'error' => 'Unauthorized action.'], 403);
            }
        }

        try {
            $doc->delete();
            return response()->json(['success'=> true, 'message'=> 'Document removed correctly from work!!'], 200);
        } catch (Exception $e) {
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 500);
        }
      }
  }
}
