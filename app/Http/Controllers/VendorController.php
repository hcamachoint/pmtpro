<?php

namespace App\Http\Controllers;
use Session;
use Redirect;
use Illuminate\Http\Request;
use App\Vendor;
use App\City;

class VendorController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $user = auth()->user();
    return view('vendor.index', ['vendors' => $user->vendor]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    $cities = City::all('title', 'id');
    return view('vendor.create', ['cities' => $cities->sortBy('title')->pluck('title', 'id')]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'name' => 'required | string | max:50',
      'phone' => 'required | digits_between:10,20',
      'address' => 'required | string',
      'city_id' => 'required | integer | between:1,20',
      'zip' => request()->has('zip') ? '' : 'required | string | between:1,10'
    ]);
    try {
      $vendor = new Vendor;
      $vendor->fill($request->all());
      $vendor->user_id = auth()->user()->id;
      $vendor->save();
      return Redirect()->route('vendor.index')->with('message', 'Vendor registered correctly!');
    } catch (\Exception $e) {
      return $e;
      Session::flash('message-error', 'An error occurred while trying to process your request.');
      return Redirect::back();
    }

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $cities = City::all('title', 'id');
    $vendor = Vendor::find($id);
    return view('vendor.edit', ['vendor' => $vendor, 'cities' => $cities->sortBy('title')->pluck('title', 'id')]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'name' => 'required | string | max:50',
      'phone' => 'required | digits_between:10,20',
      'address' => 'required | string',
      'city_id' => request()->has('city_id') ? '' : 'required | integer | between:1,20',
      'zip' => request()->has('zip') ? '' : 'required | string | between:1,10'
    ]);
    try {
      $vendor = Vendor::find($id);
      if (auth()->user()->id === $vendor->user_id) {
        $vendor->fill($request->all());
        $vendor->save();
        return Redirect()->route('vendor.index')->with('message', 'Vendor updated correctly!');
      }else{
          return abort(403, 'Unauthorized action.');
      }
    } catch (\Exception $e) {
      Session::flash('message-error', 'An error occurred while trying to process your request.');
      return Redirect::back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    try {
      $vendor = Vendor::find($id);
      if (auth()->user()->id === $vendor->user_id) {
          $vendor->delete();
          Session::flash('message', 'Vendor deleted properly!');
          return Redirect()->route('vendor.index');
      }else{
          return abort(403, 'Unauthorized action.');
      }
    } catch (\Exception $e) {
      Session::flash('message-error', 'An error occurred while trying to process your request.');
      return Redirect::back();
    }

  }

}

?>
