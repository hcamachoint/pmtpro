<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class ReportController extends Controller
{
  public function index()
  {
      $user = auth()->user();
      $projects = count($user->project);
      return view('reports', ['projects' => $projects]);
  }

  public function projectResources(Request $request)
  {
    $user = auth()->user();
    $date = Carbon::now();
    $actpro = count($user->project->where('status', 1));
    $clopro = count($user->project->where('status', 2));
    $finpro = count($user->project->where('status', 3));

    $data =  [
      'user' => $user,
      'date' => $date->toDateTimeString(),
      'actpro' => $actpro,
      'clopro' => $clopro,
      'finpro' => $finpro
    ];

    $pdf = \PDF::loadView('pdf.resources', compact('data'));
    return $pdf->stream();
  }
}
