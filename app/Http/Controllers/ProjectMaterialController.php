<?php

namespace App\Http\Controllers;

use Session;
use Redirect;
use Illuminate\Http\Request;
use App\Project;
use App\ProjectLabor;
use App\ProjectMaterial;
use Illuminate\Support\Facades\Input;

class ProjectMaterialController extends Controller
{
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'material_description.*' => 'required | string',
      /*'material_price.*' => 'required | numeric | between:0,999999.99',
      'material_count.*' => 'required | numeric | between:0,999999.99',
      'material_attachment.*' => request()->has('material_attachment.*') ? '' : 'mimes:jpg,JPG,jpeg,JPEG,png,PNG,doc,DOC,docx,DOCX,pdf,PDF,odt,ODT | max:4096',*/
    ]);

    try {
      $project = Project::find($id);
    } catch (\Exception $e) {
      return Redirect::back()->with('message-error', 'An error occurred while trying to process your request.');
    }

    if (empty($project)) {
      return Redirect::back()->with('message-error', 'Project material not found!');
    }elseif ($project->user_id != auth()->user()->id) {
      return Redirect::back()->with('message-error', 'Unauthorized action!');
    }elseif ($project->status == 2 || $project->status == 3) {
      return Redirect::back()->with('message-error', 'You can not edit a closed / finalized project!');
    }else{
      foreach ($project->materials as $material => $value) {
        $value->delete();
      }
      if (request()->has('material_description')) {
        foreach($request->material_description as $index => $value) {
          $material = new ProjectMaterial;
          if (request()->has('material_attachment')) {
            $file = $request->material_attachment[$index];
            $fileName = $file->getClientOriginalName();
            $filePath = url('uploads/documents/'.$fileName);
            $destinationPath = 'uploads/documents';
            if($file->move($destinationPath,$fileName)) {
              $material->attachment = $filePath;
            }else {
              return Redirect()->back()->with('message-error', 'Unable to upload files to server!');
            }
          }
          $material->project_id = $project->id;
          $material->description = $request->material_description[$index];
          $material->price = floatval(str_replace(',', '', $request->material_price[$index]));
          $material->count = floatval(str_replace(',', '', $request->material_count[$index]));
          $material->save();
        }
        return Redirect::back()->with('message', 'Project material updated correctly!');
      }else {
        return Redirect::back()->with('message', 'Project material updated correctly!');
      }
    }
  }
}
