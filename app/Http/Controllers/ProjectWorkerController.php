<?php

namespace App\Http\Controllers;

use Session;
use Redirect;
use Illuminate\Http\Request;
use App\Project;
use App\ProjectWorker;
use App\ProjectMaterial;
use Illuminate\Support\Facades\Input;

class ProjectWorkerController extends Controller
{
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'worker_id.*' => 'required | string',
      /*'worker_price.*' => 'required | numeric | between:0,999999.99',
      'worker_hours.*' => 'required | numeric | between:0,999999.99',*/
    ]);

    try {
      $project = Project::find($id);
    } catch (\Exception $e) {
      return Redirect::back()->with('message-error', 'An error occurred while trying to process your request.');
    }

    if (empty($project)) {
      return Redirect::back()->with('message-error', 'Project material not found!');
    }elseif ($project->user_id != auth()->user()->id) {
      return Redirect::back()->with('message-error', 'Unauthorized action!');
    }elseif ($project->status == 2 || $project->status == 3) {
      return Redirect::back()->with('message-error', 'You can not edit a closed / finalized project!');
    }else{
      foreach ($project->workers as $worker => $value) {
        $value->delete();
      }
      if (request()->has('worker_id')) {
        foreach($request->worker_id as $index => $value) {
            $worker = new ProjectWorker;
            $worker->project_id = $project->id;
            $worker->worker_id = $request->worker_id[$index];
            $worker->price = floatval(str_replace(',', '', $request->worker_price[$index]));
            $worker->hour = floatval(str_replace(',', '', $request->worker_hours[$index]));
            $worker->save();
        }
        return Redirect::back()->with('message', 'Project material updated correctly!');
      }else {
        return Redirect::back()->with('message', 'Project material updated correctly!');
      }
    }
  }
}

?>
