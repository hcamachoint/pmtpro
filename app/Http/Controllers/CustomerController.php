<?php

namespace App\Http\Controllers;

use Session;
use Redirect;
use Image;
use Illuminate\Http\Request;
use App\Customer;
use App\City;

class CustomerController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index()
  {
    $user = auth()->user();
    return view('customer.index', ['customers' => $user->customer]);
  }

  public function create()
  {
    $cities = City::all('title', 'id');
    return view('customer.create', ['cities' => $cities->sortBy('title')->pluck('title', 'id')]);
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'name' => 'required | string | max:50',
      'address' => 'required | string',
      'address2' => request()->has('address2') ? '' : 'required | string',
      'city' => 'required | integer | between:1,20',
      'zip' => request()->has('zip') ? '' : 'required | string | between:1,10',
      'phone' => request()->has('phone') ? '' : 'required | digits_between:10,20',
      'email' => request()->has('email') ? '' : 'required | email | string | between:3,200',
    ]);
    try {
      $customer = new Customer;
      $customer->fill($request->all());
      $customer->user_id = auth()->user()->id;
      $customer->save();
      return Redirect()->route('customer.index')->with('message', 'Customer registered correctly!');
    } catch (\Exception $e) {
      return $e;
      Session::flash('message-error', 'An error occurred while trying to process your request.'. $e);
      return Redirect::back();
    }
  }

  public function show($id)
  {

  }

  public function edit($id)
  {
    $cities = City::all('title', 'id');
    $customer = Customer::find($id);
    return view('customer.edit', ['customer' => $customer, 'cities' => $cities->sortBy('title')->pluck('title', 'id')]);
  }

  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'name' => 'required | string | max:50',
      'address' => 'required | string',
      'address2' => request()->has('address2') ? '' : 'required | string',
      'city' => request()->has('city') ? '' : 'required | integer | between:1,20',
      'zip' => request()->has('zip') ? '' : 'required | string | between:1,10',
      'phone' => request()->has('phone') ? '' : 'required | digits_between:10,20',
      'email' => request()->has('email') ? '' : 'required | email | string | between:3,200',
    ]);

    try {
      $customer = Customer::find($id);
      if (auth()->user()->id === $customer->user_id) {
        $customer->fill($request->all());
        $customer->save();
        return Redirect()->route('customer.index')->with('message', 'Customer updated correctly!');
      }else{
          return abort(403, 'Unauthorized action.');
      }
    } catch (\Exception $e) {
      Session::flash('message-error', 'An error occurred while trying to process your request.');
      return Redirect::back();
    }
  }

  public function destroy($id)
  {
    try {
      $customer = Customer::find($id);
      if (auth()->user()->id === $customer->user_id) {
          if ($customer->picture != '' && $customer->picture != 'default-company.jpg') {
            unlink(public_path('uploads/customers/'.$customer->picture));
          }

          $customer->delete();
          Session::flash('message', 'Customer deleted properly!');
          return Redirect()->route('customer.index');
      }else{
          return abort(403, 'Unauthorized action.');
      }
    } catch (\Exception $e) {
      Session::flash('message-error', 'An error occurred while trying to process your request.');
      return Redirect::back();
    }
  }

  public function picture($id)
  {
    $customer = Customer::find($id);
    return view('customer.picture', ['customer' => $customer]);
  }

  public function setPicture(Request $request, $id)
  {
      $this->validate($request, [
          'picture' => 'required | dimensions:min_width=100,min_height=100 | image',
      ]);

      try {
          $customer = Customer::find($id);
          if (auth()->user()->id === $customer->user_id) {
            $picture = $request->file('picture');
            $filename = hash('ripemd160', $request->user()). '_' . time() . '.' . $picture->getClientOriginalExtension();
            Image::make($picture)->resize(300, 300)->save( public_path('uploads/customers/' . $filename ) );

            try {
                $customer->picture = $filename;
                $customer->save();
                return Redirect()->route('customer.index')->with('message', 'Photo successfully updated!');
            } catch (Exception $e) {
                Session::flash('message-error', 'An error occurred while trying to process your request');
                return Redirect::back();
            }

          }else{
              return abort(403, 'Unauthorized action.');
          }
      } catch (Exception $e) {
          Session::flash('message-error', 'An error occurred while trying to process your request');
          return Redirect::back();
      }
  }

}

?>
