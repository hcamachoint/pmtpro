<?php

namespace App\Http\Controllers;

use Session;
use Redirect;
use Illuminate\Http\Request;
use App\Merchant;

class MerchantController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $user = auth()->user();
    return view('merchant.index', ['merchants' => $user->merchant]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    return view('merchant.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'name' => 'required | string | max:50',
      'phone' => 'required | digits_between:10,20',
      'address' => 'required | string'
    ]);
    try {
      $merchant = new Merchant;
      $merchant->fill($request->all());
      $merchant->user_id = auth()->user()->id;
      $merchant->save();
      return Redirect()->route('merchant.index')->with('message', 'Merchant registered correctly!');
    } catch (\Exception $e) {
      Session::flash('message-error', 'An error occurred while trying to process your request.');
      return Redirect::back();
    }

  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $merchant = Merchant::find($id);
    return view('merchant.edit', ['merchant' => $merchant]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'name' => 'required | string | max:50',
      'phone' => 'required | digits_between:10,20',
      'address' => 'required | string'
    ]);
    try {
      $merchant = Merchant::find($id);
      if (auth()->user()->id === $merchant->user_id) {
        $merchant->fill($request->all());
        $merchant->save();
        return Redirect()->route('merchant.index')->with('message', 'Merchant updated correctly!');
      }else{
          return abort(403, 'Unauthorized action.');
      }
    } catch (\Exception $e) {
      Session::flash('message-error', 'An error occurred while trying to process your request.');
      return Redirect::back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    try {
      $merchant = Merchant::find($id);
      if (auth()->user()->id === $merchant->user_id) {
          $merchant->delete();
          Session::flash('message', 'Merchant deleted properly!');
          return Redirect()->route('merchant.index');
      }else{
          return abort(403, 'Unauthorized action.');
      }
    } catch (\Exception $e) {
      Session::flash('message-error', 'An error occurred while trying to process your request.');
      return Redirect::back();
    }

  }

}

?>
