<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = auth()->user();
        $projects = count($user->project);
        $totalBudget = 0;
        $totalWorker = 0;
        $totalMaterial = 0;
        $totalInvested = 0;

        //ESTADISTICAS
        if ($projects != 0) {
          foreach ($user->project as $key => $pro) {
            $totalBudget += $pro->budget;
            foreach ($pro->materials as $key => $pm) {
              $totalMaterial += ($pm->price * $pm->count);
              $totalInvested += ($pm->price * $pm->count);
            }
            foreach ($pro->workers as $key => $pl) {
              $totalWorker += ($pl->price * $pl->hour);
              $totalInvested += ($pl->price * $pl->hour);
            }
          }
        }

        return view('home', ['projects' => $projects, 'totalb' => $totalBudget, 'totalm' => $totalMaterial, 'totalw' => $totalWorker, 'totali' => $totalInvested]);
    }
}
