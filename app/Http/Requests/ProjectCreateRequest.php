<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'sdate' => 'required | date | before_or_equal:edate',
          'edate' => 'required | date',
          'budget' => 'required | numeric | between:0,999999999.99',
          'name' => 'required | string',
          'description' => 'required | string',
          'material_description.*' => request()->has('material_description.*') ? '' : 'required | string',
          'material_price.*' => request()->has('material_description.*') ? '' : 'required | numeric | between:0,999999.99',
          'material_count.*' => request()->has('material_description.*') ? '' : 'required | numeric | between:0,999999.99',
          'material_attachment.*' => request()->has('material_attachment.*') ? '' : 'mimes:jpg,JPG,jpeg,JPEG,png,PNG,doc,DOC,docx,DOCX,pdf,PDF,odt,ODT | max:4096',
          'labor_id.*' => request()->has('labor_id.*') ? '' : 'required | string',
          'labor_price.*' => request()->has('labor_id.*') ? '' : 'required | numeric | between:0,999999.99',
          'labor_hours.*' => request()->has('labor_id.*') ? '' : 'required | numeric | between:0,999999.99',
          'customer_id' => request()->has('customer_id') ? '' : 'integer',
          'cname' => request()->has('cname') ? '' : 'string | max:50',
          'caddress' => request()->has('caddress') ? '' : 'string',
          'ccity' => request()->has('ccity') ? '' : 'integer | between:1,20'
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            'budget' => floatval(str_replace(',', '', $this->input('budget'))),
        ]);
    }
}
