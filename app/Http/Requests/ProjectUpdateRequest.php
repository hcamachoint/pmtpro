<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'customer_id' => 'required | integer',
          'sdate' => 'required | date | before_or_equal:edate',
          'edate' => 'required | date',
          'budget' => 'required | numeric | between:0,999999999.99',
          'name' => 'required | string',
          'description' => 'required | string',
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            'budget' => floatval(str_replace(',', '', $this->input('budget'))),
        ]);
    }
}
