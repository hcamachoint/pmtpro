<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectVendors extends Model 
{

    protected $table = 'project_vendors';
    public $timestamps = true;
    protected $fillable = array('product', 'price', 'attachment');

}