<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/logout', 'UserController@logout')->name('logout');
Route::resource('vendor', 'VendorController');
Route::resource('projectmaterial', 'ProjectMaterialController');
Route::resource('projectworker', 'ProjectWorkerController');

Route::get('/project/old/', 'ProjectController@old')->name('project.old');
Route::put('/project/finish/{id}', 'ProjectController@finish')->name('project.finish');
Route::put('/project/close/{id}', 'ProjectController@close')->name('project.close');
Route::put('/project/open/{id}', 'ProjectController@open')->name('project.open');
Route::post('/project/worker/', 'ProjectController@worker');
Route::post('/project/customer/', 'ProjectController@customer');
Route::get('project/pdf/{id}', 'ProjectController@pdf')->name('project.pdf');
Route::resource('project', 'ProjectController');

Route::get('/worker/picture/{id}', 'WorkerController@picture')->name('worker.picture');
Route::post('/worker/picture/{id}', 'WorkerController@setPicture')->name('worker.setpicture');
Route::resource('worker', 'WorkerController');

Route::resource('customer', 'CustomerController');
Route::get('/customer/picture/{id}', 'CustomerController@picture')->name('customer.picture');
Route::post('customer/picture/{id}', 'CustomerController@setPicture')->name('customer.setpicture');

/*Route::group(['prefix' => 'document', 'middleware' => ['auth']], function () {
    Route::get('/', 'Api\DocumentController@list');
    Route::get('/{cod}', 'Api\DocumentController@view');
    Route::post('/{id}', 'Api\DocumentController@store');
    Route::put('/{id}/{cod}', 'Api\DocumentController@update');
    Route::delete('/{id}/{cod}', 'Api\DocumentController@destroy');
});*/

/*Route::group(['prefix' => 'reports'], function() {
	Route::get('/', 'ReportController@index')->name('reports');
  Route::get('/project-resources', 'ReportController@projectResources')->name('reports.resources');
});*/

/*
Route::resource('user', 'UserController');

Route::resource('projectlabors', 'ProjectLaborsController');
Route::resource('projectmerchants', 'ProjectMerchantsController');
Route::resource('projectvendors', 'ProjectVendorsController');
Route::resource('projectbudgets', 'ProjectBudgetsController');
*/
